#ifndef FRAME_H
#define FRAME_H

#include <GL/glew.h>
#include <cmath>
// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

using namespace glm;

#include "shader.hpp"

#include "ball.h"
#include <vector>

#include "functions.h"

class Frame
{
public:
    Frame();

    ~Frame();

    void init();
    void draw();
    void clean();

    bool lifeLost;

    std::vector<vec2> checkColisions(Ball ball_obj);


private:
    float frameA, frameB;
    float frameS, frameH, frameR;

    vec2 frameVertices[6];
    vec2 frameNormals[6];

    GLuint frameVA;

    GLuint frameVerticesVB;
    GLuint frameColorVB;

    GLuint HexFrameShader;
};

#endif // FRAME_H
