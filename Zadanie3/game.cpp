#include "game.h"

#include <GL/glew.h>
#include <cstdio>
#include <stdio.h>
#include <ctime>
#include <cmath>
#include <thread>
// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>


using namespace glm;

#include "shader.hpp"


Game::Game()
{
}

void Game::initGame()
{
    frame_obj.init();
    bg_obj.init();
    paddle_obj.init();
    ball_obj.init();
    bricks_obj.init();

    pause = false;
    ballMoves = false;
    gameOver = false;

    leftKeyReleased = true;
    rightKeyReleased = true;
    spaceKeyReleased = true;
    upKeyReleased = true;
    lifesNumber = 3;
}

bool Game::initGLFW()
{
    // Initialise GLFW

    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        getchar();
        return false;
    }

    return true;
}

bool Game::createWindow()
{
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    this->window = glfwCreateWindow( 800, 800, "Adam Sawicki", NULL, NULL);
    if( this->window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        getchar();
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(this->window);

    return true;
}

bool Game::initGLEW()
{
    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        return false;
    }

    return true;
}


void Game::loop()
{
    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    // Black background
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);


    //glGenVertexArrays(1, &VertexArrayID);
    //glBindVertexArray(VertexArrayID);

    double startTime = glfwGetTime();
    int nbFrames = 0;
    double lastTime = glfwGetTime();
    do{

        // Clear the screen
        glClear( GL_COLOR_BUFFER_BIT );

        double currentTime = glfwGetTime();
        nbFrames++;
        if ( currentTime - startTime >= 1.0 ){ // If last prinf() was more than 1 sec ago
            // printf and reset timer
            //printf("%f ms/frame\n", 1000.0/double(nbFrames));
            //printf("FPS: %d\n", nbFrames);
            nbFrames = 0;
            startTime += 1.0;
        }


        if(!gameOver)
        {
            handleInput(currentTime - lastTime);
            int samples = 10;
            for(int i=0; i<samples; i++)
            {
                if(!pause)
                {
                    if(lifesNumber >= 0)
                    {
                        if(ballMoves)
                        {
                            ball_obj.rotate((currentTime - lastTime)/samples);
                            ball_obj.move((currentTime - lastTime)/samples);
                            checkCollisions();
                        }
                        else
                        {
                            ballOnPaddle();
                        }
                    }
                }
            }
        }

        lastTime = glfwGetTime();
        bg_obj.draw();

        if(lifesNumber >= 0)
            ball_obj.draw();

        bricks_obj.draw();
        paddle_obj.draw();
        frame_obj.draw();
        ball_obj.drawLifes(lifesNumber);



        //swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0);
}

void Game::clean()
{
    // Cleanup VBO
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteVertexArrays(1, &VertexArrayID);
    //glDeleteProgram(programID);

    bg_obj.clean();
    frame_obj.clean();
    paddle_obj.clean();
    bricks_obj.clean();

    // Close OpenGL window and terminate GLFW
    glfwTerminate();
}

void Game::handleInput(double frameTime)
{
    if(!pause)
    {

        if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && rightKeyReleased)
        {
            leftKeyReleased = false;

            paddle_obj.moveLeft(frameTime);
        }
        else if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_RELEASE)
        {
            leftKeyReleased = true;
        }

        if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && leftKeyReleased)
        {
            rightKeyReleased = false;
            paddle_obj.moveRight(frameTime);
        }
        else if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_RELEASE)
        {
            rightKeyReleased = true;
        }

        if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && leftKeyReleased)
        {
            upKeyReleased = false;
            if(ballMoves == false)
            {
                ball_obj.startMoving();
            }

            ballMoves = true;
        }
        else if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_RELEASE)
        {
            rightKeyReleased = true;
        }


    }

    if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && spaceKeyReleased == true)
    {
        spaceKeyReleased = false;
    }
    else if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE && spaceKeyReleased == false)
    {
        spaceKeyReleased = true;
        pause = !pause;
    }
}

void Game::checkCollisions()
{
    std::vector<vec2> colisionNormals;
    colisionNormals = frame_obj.checkColisions(ball_obj);

    std::vector<vec2> paddleColisionNormals = paddle_obj.checkColisions(ball_obj);

    std::vector<vec2> bricksColisionNormals = bricks_obj.checkColisions(ball_obj);

    for(int i=0; i<paddleColisionNormals.size(); i++)
    {
        colisionNormals.push_back(paddleColisionNormals[i]);
    }

    for(int i=0; i<bricksColisionNormals.size(); i++)
    {
        colisionNormals.push_back(bricksColisionNormals[i]);
    }

    vec2 ans = vec2(0.0, 0.0);

    if(colisionNormals.size() > 0)
    {
        for(int i=0; i<colisionNormals.size(); i++)
        {
            ans+= colisionNormals[i];

        }
    }

    if(colisionNormals.size() > 0)
    {
          vec2 ref = reflect(ball_obj.speed, normalize(ans));

          if(glm::dot(ball_obj.speed, normalize(ans)) < 0)
          {
              if((ref.x == 0.0 && ref.y == 0.0 ) || (ans.x == 0.0 && ans.y == 0))
              {
                  ball_obj.speed.x *=-1;
                  ball_obj.speed.y *=-1;
              }
              else
              {
                  ball_obj.speed = ref;
              }

              if(paddle_obj.colision == 1)
              {
                  if(glm::length(ball_obj.speed - paddle_obj.speed) < 0.5f)
                    ball_obj.speed -= paddle_obj.speed;
              }
              else if(paddle_obj.colision == 2)
              {
                  if(glm::length(ball_obj.speed + paddle_obj.speed) < 0.5f)
                    ball_obj.speed += paddle_obj.speed;
              }
          }
    }

    if(frame_obj.lifeLost)
    {
        lifesNumber--;
        if(lifesNumber == -1 && bricks_obj.bricksNumber() > 0 && !gameOver)
        {
            gameOver= true;
            printf("Przegrales! Koniec gry!\n");
        }
        frame_obj.lifeLost = false;
        ballMoves = false;
    }

    if(bricks_obj.bricksNumber() == 0 && !gameOver)
    {
        gameOver = true;
        printf("Gratulacje! Wygrana!\n");
    }

}

void Game::ballOnPaddle()
{
    ball_obj.center.x = paddle_obj.center.x;
    ball_obj.center.y = paddle_obj.center.y + paddle_obj.height/2 + ball_obj.radius;
}

