#include "brick.h"

Brick::Brick(GLuint BrickVA, GLuint BrickShader,
             float width, float height,
             vec2 center,
             int color)
{
    this->BrickVA = BrickVA;
    this->BrickShader = BrickShader;
    this -> width = width;
    this -> height = height;
    this -> center = center;
    this -> color = color;

    init();
}

void Brick::init()
{
    scale = vec2(width / 2, height / 2);

    //if we define dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx)
    brickNormals[0] = normalize(vec2(0, width));   //top
    brickNormals[1] = normalize(vec2(height , 0));   //right
    brickNormals[2] = normalize(vec2(0 , -width));   //bottom
    brickNormals[3] = normalize(vec2(-height , 0));   //left
}

Brick::~Brick()
{

}

void Brick::draw()
{
    GLint uniScale = glGetUniformLocation(BrickShader, "scaling");
    GLint uniLine = glGetUniformLocation(BrickShader, "isLine");
    GLint uniTrans = glGetUniformLocation(BrickShader, "translation");

    vec2 trans = vec2(0.0f, 0.0f) + center;

    glUniform2f(uniTrans, trans.x, trans.y);

    glUniform2f(uniScale, scale.x, scale.y);
    glUniform1i(uniLine, false);

    glDrawArrays(GL_TRIANGLE_FAN, color*6, 6);

    glUniform1i(uniLine, true);

    glDrawArrays(GL_LINE_LOOP, 1, 4);
}

std::vector<vec2> Brick::checkColisions(Ball ball_obj)
{
    std::vector<vec2> normals;

    vec2 upRight =      center + vec2(width/2, height/2);
    vec2 upLeft =       center + vec2(-width/2, height/2);
    vec2 bottomRight =  center + vec2(width/2, -height/2);
    vec2 bottomLeft =   center + vec2(-width/2, -height/2);

    if(colisionBetweenSectionAndBall(upLeft, upRight, ball_obj))            //top
    {
        normals.push_back(brickNormals[0]);
    }

    if(colisionBetweenSectionAndBall(upRight, bottomRight, ball_obj))       //right
    {
        normals.push_back(brickNormals[1]);
    }

    if(colisionBetweenSectionAndBall(bottomRight, bottomLeft, ball_obj))    //bottom
    {
        normals.push_back(brickNormals[2]);
    }

    if(colisionBetweenSectionAndBall(bottomLeft, upLeft, ball_obj))         //left
    {
        normals.push_back(brickNormals[3]);
    }

    return normals;
}
