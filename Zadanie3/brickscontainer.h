#ifndef BRICKSCONTAINER_H
#define BRICKSCONTAINER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cstdio>

#include <glm/glm.hpp>

using namespace glm;

#include "shader.hpp"

#include "brick.h"
#include "ball.h"
#include <vector>

class BricksContainer
{
public:
    BricksContainer();
    void init();
    void draw();
    void clean();
    std::vector<vec2> checkColisions(Ball ball_obj);

    int bricksNumber();

private:
    GLuint BrickVA;

    GLuint BrickVerticesVB;
    GLuint BrickColorVB;

    GLuint BrickShader;

    std::vector<Brick> bricks;


};

#endif // BRICKSCONTAINER_H
