#ifndef PADDLE_H
#define PADDLE_H

#include <GL/glew.h>
// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

using namespace glm;

#include "shader.hpp"
#include "ball.h"
#include <vector>

#include "functions.h"

class Paddle
{
public:
    Paddle();
    ~Paddle();
    void init();
    void draw();
    void clean();

    void moveLeft(double frameTime);
    void moveRight(double frameTime);
    std::vector<vec2> checkColisions(Ball ball_obj);

    int colision;
    vec2 speed;

    vec2 center;

    float width, height;

private:

    vec2 paddleNormals[4];

    GLuint PaddleVA;

    GLuint PaddleVerticesVB;
    GLuint PaddleColorVB;

    GLuint PaddleShader;

    vec2 scale;


    float minX, maxX;
};

#endif // PADDLE_H
