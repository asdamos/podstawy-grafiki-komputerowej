#include "brickscontainer.h"

#define PI 3.14159265


BricksContainer::BricksContainer()
{
}

void BricksContainer::init()
{

    BrickShader = LoadShaders("brickVS.glsl", "brickFS.glsl" );

    glGenVertexArrays(1, &BrickVA);
    glBindVertexArray(BrickVA);

    glGenBuffers(1, &BrickVerticesVB);
    glBindBuffer(GL_ARRAY_BUFFER, BrickVerticesVB);

    GLfloat BrickVerticesBufferData[] = {
        0.0f, 0.0f,     //center
        1.0f, 1.0f,     //up right
        1.0f, -1.0f,    //down right
        -1.0f, -1.0f,   //down left
        -1.0f, 1.0f,    //up left
        1.0f, 1.0f,      //up right
        0.0f, 0.0f,     //center
        1.0f, 1.0f,     //up right
        1.0f, -1.0f,    //down right
        -1.0f, -1.0f,   //down left
        -1.0f, 1.0f,    //up left
        1.0f, 1.0f,      //up right
        0.0f, 0.0f,     //center
        1.0f, 1.0f,     //up right
        1.0f, -1.0f,    //down right
        -1.0f, -1.0f,   //down left
        -1.0f, 1.0f,    //up left
        1.0f, 1.0f,      //up right
        0.0f, 0.0f,     //center
        1.0f, 1.0f,     //up right
        1.0f, -1.0f,    //down right
        -1.0f, -1.0f,   //down left
        -1.0f, 1.0f,    //up left
        1.0f, 1.0f,      //up right
        0.0f, 0.0f,     //center
        1.0f, 1.0f,     //up right
        1.0f, -1.0f,    //down right
        -1.0f, -1.0f,   //down left
        -1.0f, 1.0f,    //up left
        1.0f, 1.0f,      //up right
        0.0f, 0.0f,     //center
        1.0f, 1.0f,     //up right
        1.0f, -1.0f,    //down right
        -1.0f, -1.0f,   //down left
        -1.0f, 1.0f,    //up left
        1.0f, 1.0f      //up right
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof(BrickVerticesBufferData), BrickVerticesBufferData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, BrickVerticesVB);
    glVertexAttribPointer(
                0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                2,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );

    glDisableVertexAttribArray(0);

    glGenBuffers(1, &BrickColorVB);
    glBindBuffer(GL_ARRAY_BUFFER, BrickColorVB);

    GLfloat BrickColorBufferData[] = {
        0.85f, 0.85f, 0.85f,   //white
        1.0f, 0.0f, 0.0f,      //red
        1.0f, 0.0f, 0.0f,      //red
        1.0f, 0.0f, 0.0f,      //red
        1.0f, 0.0f, 0.0f,      //red
        1.0f, 0.0f, 0.0f,      //red
        0.85f, 0.85f, 0.85f,   //white
        0.0f, 1.0f, 0.0f,      //green
        0.0f, 1.0f, 0.0f,      //green
        0.0f, 1.0f, 0.0f,      //green
        0.0f, 1.0f, 0.0f,      //green
        0.0f, 1.0f, 0.0f,      //green
        0.85f, 0.85f, 0.85f,   //white
        0.0f, 0.0f, 1.0f,      //blue
        0.0f, 0.0f, 1.0f,      //blue
        0.0f, 0.0f, 1.0f,      //blue
        0.0f, 0.0f, 1.0f,      //blue
        0.0f, 0.0f, 1.0f,      //blue
        0.85f, 0.85f, 0.85f,   //white
        1.0f, 0.0f, 1.0f,      //magenta
        1.0f, 0.0f, 1.0f,      //magenta
        1.0f, 0.0f, 1.0f,      //magenta
        1.0f, 0.0f, 1.0f,      //magenta
        1.0f, 0.0f, 1.0f,      //magenta
        0.85f, 0.85f, 0.85f,   //white
        1.0f, 1.0f, 0.0f,      //yellow
        1.0f, 1.0f, 0.0f,      //yellow
        1.0f, 1.0f, 0.0f,      //yellow
        1.0f, 1.0f, 0.0f,      //yellow
        1.0f, 1.0f, 0.0f,      //yellow
        0.85f, 0.85f, 0.85f,   //white
        0.0f, 1.0f, 1.0f,      //aqua
        0.0f, 1.0f, 1.0f,      //aqua
        0.0f, 1.0f, 1.0f,      //aqua
        0.0f, 1.0f, 1.0f,      //aqua
        0.0f, 1.0f, 1.0f,      //aqua
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof(BrickColorBufferData), BrickColorBufferData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, BrickColorVB);
    glVertexAttribPointer(
        1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
    );

    glDisableVertexAttribArray(1);

    //make bricks

    for(int i=0; i<6; i++)
    {
        bricks.push_back(Brick(BrickVA, BrickShader,
                               0.2f, 0.1f,
                               vec2(-0.5f + i*0.2f, 0.4f),
                               i%6));
    }
    for(int i=0; i<12; i++)
    {
        bricks.push_back(Brick(BrickVA, BrickShader,
                               0.1f, 0.1f,
                               vec2(-0.55f + i*0.1f, 0.5f),
                               (i/2) % 6));
    }
    for(int i=0; i<6; i++)
    {
        bricks.push_back(Brick(BrickVA, BrickShader,
                               0.2f, 0.1f,
                               vec2(-0.5f + i*0.2f, 0.6f),
                               (i+2) % 6));
    }

}

std::vector<vec2> BricksContainer::checkColisions(Ball ball_obj)
{
    std::vector<vec2> normals;
    int n = bricks.size();
    for(int i=0; i<n; i++)
    {
        std::vector<vec2> brickNormals = bricks[i].checkColisions(ball_obj);
        for(int j=0; j<brickNormals.size(); j++)
        {
            normals.push_back(brickNormals[j]);
        }
        if(brickNormals.size() > 0) //there was colision so we delete this brick
        {
            bricks.erase(bricks.begin() + i);
            n--;
            i--;
        }
    }
    return normals;
}

void BricksContainer::clean()
{
    glDeleteBuffers(1, &BrickVerticesVB);
    glDeleteBuffers(1, &BrickColorVB);

    glDeleteVertexArrays(1, &BrickVA);
    glDeleteProgram(BrickShader);
}

void BricksContainer::draw()
{
    glBindVertexArray(BrickVA);

    glUseProgram(BrickShader);

    glEnableVertexAttribArray(0);   //vertices
    glEnableVertexAttribArray(1);   //colors

    for(int i=0; i<bricks.size(); i++)
    {
        bricks[i].draw();
    }

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}

int BricksContainer::bricksNumber()
{
    return bricks.size();
}
