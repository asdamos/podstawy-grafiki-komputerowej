#ifndef BRICK_H
#define BRICK_H

#include <GL/glew.h>
// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

using namespace glm;

#include "shader.hpp"
#include "ball.h"
#include "vector"
#include "functions.h"


class Brick
{
public:
    Brick(GLuint BrickVA, GLuint BrickShader, float width, float height, vec2 center, int color);
    ~Brick();
    void init();
    void draw();

    std::vector<vec2> checkColisions(Ball ball_obj);

private:

    bool destroyed;
    vec2 brickNormals[4];
    int color;

    GLuint BrickVA;
    GLuint BrickShader;

    vec2 scale;
    vec2 center;

    float width, height;
};

#endif // BRICK_H
