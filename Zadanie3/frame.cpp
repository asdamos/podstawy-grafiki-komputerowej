#include "frame.h"

#define PI 3.14159265


Frame::Frame()
{
}

void Frame::init()
{
    lifeLost = false;

    HexFrameShader = LoadShaders("HexFrameVS.glsl", "HexFrameFS.glsl" );
    frameA = 1.8;
    frameB = 1.8;
    frameR = frameA / 2.0;

    frameS = frameR / cos(30 * PI / 180.0);
    //frameH = frameS * sin(30 * PI / 180.0);
    frameH = (frameB - frameS) / 2;


    frameVertices[0] = vec2(0.9f - frameH, 0.9f);   //top right
    frameVertices[1] = vec2(0.9f, 0.0f);            //right
    frameVertices[2] = vec2(0.9f - frameH, -0.9f);  //down right
    frameVertices[3] = vec2(-0.9f + frameH, -0.9f); //down left
    frameVertices[4] = vec2(-0.9f, 0.0f);           //left
    frameVertices[5] = vec2(-0.9f + frameH, 0.9f);  //top left

    //if we define dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx)

    frameNormals[0] = normalize(vec2(frameVertices[0].y - frameVertices[5].y, frameVertices[5].x - frameVertices[0].x));   //top
    frameNormals[1] = normalize(vec2(frameVertices[1].y - frameVertices[0].y, frameVertices[0].x - frameVertices[1].x));   //upper right
    frameNormals[2] = normalize(vec2(frameVertices[2].y - frameVertices[1].y, frameVertices[1].x - frameVertices[2].x));   //lower right
    frameNormals[3] = normalize(vec2(frameVertices[3].y - frameVertices[2].y, frameVertices[2].x - frameVertices[3].x));   //down
    frameNormals[4] = normalize(vec2(frameVertices[4].y - frameVertices[3].y, frameVertices[3].x - frameVertices[4].x));   //lower left
    frameNormals[5] = normalize(vec2(frameVertices[5].y - frameVertices[4].y, frameVertices[4].x - frameVertices[5].x));   //upper left


    glGenVertexArrays(1, &frameVA);
    glBindVertexArray(frameVA);

    glGenBuffers(1, &frameVerticesVB);
    glBindBuffer(GL_ARRAY_BUFFER, frameVerticesVB);

    GLfloat frameVerticesBufferData[] = {
        //Hexagonal frame
        -1.0f, 1.0f,            //-1 1
        1.0f, 1.0f,             //1 1
        0.9f - frameH, 0.9f,    //top right
        -0.9f + frameH, 0.9f,   //top left
        -0.9f, 0.0f,            //left

        -1.0f, -1.0f,           // -1 -1
        -0.9f, 0.0f,            //left
        -0.9f + frameH, -0.9f,  //down left
        0.9f - frameH, -0.9f,   //down right
        1.0f, -1.0f,            //1 -1

        0.9f, 0.0f,             //right
        0.9f - frameH, 0.9f,    //top right
        1.0f, 1.0f,             //1 1
        1.0f, -1.0f,            //1 -1
        0.9f - frameH, -0.9f    //down right
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof(frameVerticesBufferData), frameVerticesBufferData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, frameVerticesVB);
    glVertexAttribPointer(
                0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                2,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );

    glDisableVertexAttribArray(0);

    glGenBuffers(1, &frameColorVB);
    glBindBuffer(GL_ARRAY_BUFFER, frameColorVB);

    GLfloat frameColorBufferData[] = {
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f,
        0.0f, 0.0f, 0.8f
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof(frameColorBufferData), frameColorBufferData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, frameColorVB);
    glVertexAttribPointer(
        1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
    );

    glDisableVertexAttribArray(1);
}


Frame::~Frame()
{
}

void Frame::clean()
{
    glDeleteBuffers(1, &frameVerticesVB);
    glDeleteBuffers(1, &frameColorVB);

    glDeleteVertexArrays(1, &frameVA);
    glDeleteProgram(HexFrameShader);
}

void Frame::draw()
{
    glBindVertexArray(frameVA);

    glUseProgram(HexFrameShader);

    glEnableVertexAttribArray(0);   //vertices
    glEnableVertexAttribArray(1);   //colors

    glDrawArrays(GL_TRIANGLE_FAN, 0, 6);
    glDrawArrays(GL_TRIANGLE_FAN, 5, 5);
    glDrawArrays(GL_TRIANGLE_FAN, 10, 5);

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}

std::vector<vec2> Frame::checkColisions(Ball ball_obj)
{
    std::vector<vec2> normals;

    if(colisionBetweenSectionAndBall(frameVertices[5], frameVertices[0], ball_obj)) //top
    {
        normals.push_back(frameNormals[0]);
    }

    if(colisionBetweenSectionAndBall(frameVertices[0], frameVertices[1], ball_obj)) //upper right
    {
        normals.push_back(frameNormals[1]);
    }

    if(colisionBetweenSectionAndBall(frameVertices[1], frameVertices[2], ball_obj)) //lower right
    {
        normals.push_back(frameNormals[2]);
    }

    if(colisionBetweenSectionAndBall(frameVertices[2], frameVertices[3], ball_obj)) //bottom
    {
        normals.push_back(frameNormals[3]);
        lifeLost = true;
    }

    if(colisionBetweenSectionAndBall(frameVertices[3], frameVertices[4], ball_obj)) //lower left
    {
        normals.push_back(frameNormals[4]);
    }

    if(colisionBetweenSectionAndBall(frameVertices[4], frameVertices[5], ball_obj)) //upper left
    {
        normals.push_back(frameNormals[5]);
    }

    return normals;
}

