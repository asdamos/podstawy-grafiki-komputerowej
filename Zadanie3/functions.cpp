#include "functions.h"



float distance(vec2 P1, vec2 P2, vec2 center)
{
    float ans = (P2.y - P1.y)*center.x - (P2.x - P1.x) * center.y + P2.x * P1.y - P2.y * P1.x;
    ans = abs(ans);
    ans = ans / sqrt ( (P2.y - P1.y)*(P2.y - P1.y) + (P2.x - P1.x)*(P2.x - P1.x) );
    return ans;
}


bool colisionBetweenSectionAndBall(vec2 P1, vec2 P2, Ball ball)
{
    if( distance(P1, P2, ball.center) < ball.radius)
    {
        //points
        float x1 = 0;
        float x2 = 0;

        float y1 = 0;
        float y2 = 0;

        if( (P2.x - P1.x)  == 0)        //line is vertical
        {
            //y = b
            x1 = P1.x;
            x2 = P1.x;

            //circle: (x - circle.x)^2 + (y - circle.y)^2 = r^2
            //delta := r^2 - (x - circle.x)^2, we know that x := P1.x
            //so -(y - circle.y) = sqrt(delta) or (y - circle.y) = sqrt(delta)
            // y = -sqrt(delta) + circle.y or y = sqrt(delta) + circle.y
            float delta = ball.radius * ball.radius - (P1.x - ball.center.x)*(P1.x - ball.center.x);

            y1 = ball.center.y - sqrt(delta);
            y2 = ball.center.y + sqrt(delta);

        }
        else
        {
            //y = a * x + b
            float a = ( P2.y - P1.y ) / (P2.x - P1.x);
            float b = a * (-1 * P1.x) + P1.y;


            //Ax^2 + Bx +C = 0
            float A = (a*a +1);
            float B = 2*(a*b - a*ball.center.y - ball.center.x);
            float C = (ball.center.y * ball.center.y - ball.radius * ball.radius + ball.center.x * ball.center.x - 2*b*ball.center.y + b*b);

            float delta = B*B - 4*A*C;


            if(delta >= 0) //one or two points of intersection
            {
                x1 = (-1*B - sqrt(delta))/(2*A);
                x2 = (-1*B + sqrt(delta))/(2*A);

                y1 = a*x1 + b;
                y2 = a*x2 + b;
            }
        }

        if(x1 >= P1.x && x1 <= P2.x)
        {
            if((y1 >= P1.y && y1 <= P2.y) || (y1 >= P2.y && y1 <= P1.y))
            {
                return true;
            }
        }
        if (x1 >= P2.x && x1 <= P1.x)
        {
            if((y1 >= P1.y && y1 <= P2.y) || (y1 >= P2.y && y1 <= P1.y))
            {
                return true;
            }
        }
        if(x2 >= P1.x && x2 <= P2.x)
        {
            if((y2 >= P1.y && y2 <= P2.y) || (y2 >= P2.y && y2 <= P1.y))
            {
                return true;
            }
        }
        if (x2 >= P2.x && x2 <= P1.x)
        {
            if((y2 >= P1.y && y2 <= P2.y) || (y2 >= P2.y && y2 <= P1.y))
            {
                return true;
            }
        }
    }

    return false;
}
