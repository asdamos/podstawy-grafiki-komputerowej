#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <GL/glew.h>
#include <cmath>
// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

using namespace glm;

#include "shader.hpp"

class Background
{
public:
    Background();
    ~Background();
    void init();
    void draw();
    void clean();
private:
    float backgroundA, backgroundB;
    float backgroundS, backgroundH, backgroundR;

    GLuint backgroundVA;

    GLuint backgroundVerticesVB;
    GLuint backgroundColorVB;

    GLuint BackgroundShader;

    vec2 scale;

    int rowNumber;
    int columnNumber;
};

#endif // BACKGROUND_H
