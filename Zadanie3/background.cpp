#include "background.h"

#define PI 3.14159265


Background::Background()
{

}


void Background::init()
{
    scale = vec2(0.1f, 0.1f);

    rowNumber = 12;
    columnNumber = 10;

    BackgroundShader = LoadShaders("BackgroundVS.glsl", "BackgroundFS.glsl" );
    backgroundA = 2.0;
    backgroundB = 2.0;
    backgroundR = backgroundA / 2.0;

    backgroundS = backgroundR / cos(30 * PI / 180.0);
    backgroundH = (backgroundB - backgroundS) / 2;

    glGenVertexArrays(1, &backgroundVA);
    glBindVertexArray(backgroundVA);

    glGenBuffers(1, &backgroundVerticesVB);
    glBindBuffer(GL_ARRAY_BUFFER, backgroundVerticesVB);

    GLfloat backgroundVerticesBufferData[] = {
        0.0f, 0.0f,
        1.0f - backgroundH, 1.0f,   //top right
        1.0f, 0.0f,                 //right
        1.0f - backgroundH, -1.0f,   //down right
        -1.0f + backgroundH, -1.0f, //down left
        -1.0f, 0.0f,                //left
        -1.0f + backgroundH, 1.0f,  //top left
        1.0f - backgroundH, 1.0f    //top right
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof(backgroundVerticesBufferData), backgroundVerticesBufferData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, backgroundVerticesVB);
    glVertexAttribPointer(
                0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                2,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );

    glDisableVertexAttribArray(0);

    glGenBuffers(1, &backgroundColorVB);
    glBindBuffer(GL_ARRAY_BUFFER, backgroundColorVB);

    GLfloat backgroundColorBufferData[] = {
        0.0f, 0.0f, 0.0f,
        0.0f, 0.6f, 0.6f,
        0.0f, 0.6f, 0.6f,
        0.0f, 0.7f, 0.7f,
        0.0f, 0.7f, 0.7f,
        0.0f, 0.8f, 0.8f,
        0.0f, 0.8f, 0.8f,
        0.0f, 0.8f, 0.8f
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof(backgroundColorBufferData), backgroundColorBufferData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);


    glBindBuffer(GL_ARRAY_BUFFER, backgroundColorVB);
    glVertexAttribPointer(
        1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
    );

    glDisableVertexAttribArray(1);
}


Background::~Background()
{
}

void Background::clean()
{
    glDeleteBuffers(1, &backgroundVerticesVB);
    glDeleteBuffers(1, &backgroundColorVB);

    glDeleteVertexArrays(1, &backgroundVA);
    glDeleteProgram(BackgroundShader);
}


void Background::draw()
{
    glBindVertexArray(backgroundVA);

    glUseProgram(BackgroundShader);

    GLint uniScale = glGetUniformLocation(BackgroundShader, "scaling");
    GLint uniLine = glGetUniformLocation(BackgroundShader, "isLine");
    GLint uniTrans = glGetUniformLocation(BackgroundShader, "translation");

    glUniform2f(uniScale, scale.x, scale.y);

    glEnableVertexAttribArray(0);   //vertices
    glEnableVertexAttribArray(1);   //colors


    for(int i=0; i<rowNumber; i++)
    {
        for(int j=0; j<columnNumber; j++)
        {
            float topLeftX;
            float topLeftY;
            topLeftX = i * (backgroundH * scale.x + backgroundS * scale.x) - 1 + backgroundB*scale.x/2;
            topLeftY = j * 2 * backgroundR * scale.y + (i & 1) * backgroundR * scale.y -1 + backgroundA*scale.y/2;


            glUniform2f(uniTrans, topLeftX, topLeftY);
            glUniform1i(uniLine, false);

            glDrawArrays(GL_TRIANGLE_FAN, 0, 8);


            glUniform1i(uniLine, true);

            glDrawArrays(GL_LINE_LOOP,1,6);
        }
    }

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}
