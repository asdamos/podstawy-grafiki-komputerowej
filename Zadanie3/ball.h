#ifndef BALL_H
#define BALL_H

#include <GL/glew.h>
#include <ctime>
#include <cmath>
// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

using namespace glm;

#include "shader.hpp"

#include <cstdlib>

class Ball
{
public:
    Ball();

    ~Ball();
    void init();
    void draw();
    void clean();
    void move(double frameTime);
    void rotate(double frameTime);

    void drawLifes(int lifesNumber);

    void startMoving();

    vec2 speed;
    vec2 center;

   float radius;

private:
    GLuint BallVA;

    GLuint BallVerticesVB;
    GLuint BallColorVB;

    GLuint BallShader;

    vec2 scale;

    float angle;
    float angleSpeed;

    int verticesNumber;
};

#endif // BALL_H
