#include "ball.h"
#include <cstdio>

#define PI 3.14159265


Ball::Ball()
{
}

void Ball::init()
{
    center = vec2(0.0f, 0.0f);

    float originalRadius = 1.0f;
    radius = 0.03f;
    angle = 0;

    verticesNumber = 20;

    scale = vec2(radius / originalRadius, radius / originalRadius);




    angleSpeed = 0.5f;


    BallShader = LoadShaders("ballVS.glsl", "ballFS.glsl" );

    glGenVertexArrays(1, &BallVA);
    glBindVertexArray(BallVA);

    glGenBuffers(1, &BallVerticesVB);
    glBindBuffer(GL_ARRAY_BUFFER, BallVerticesVB);

    GLfloat BallVerticesBufferData[(verticesNumber + 2 + 4) * 2];   //additional 4 for cross
    //Center
    BallVerticesBufferData[0] = 0.0f;
    BallVerticesBufferData[1] = 0.0f;


    for(int i = 0; i < verticesNumber; i++)
    {
        float tmpAngle = 2* PI * i /verticesNumber;
        BallVerticesBufferData[2*i + 2] = cos(tmpAngle) * originalRadius;
        BallVerticesBufferData[2*i + 3] = sin(tmpAngle) * originalRadius;
    }
    BallVerticesBufferData[(verticesNumber + 2) * 2 - 2] = BallVerticesBufferData[2];
    BallVerticesBufferData[(verticesNumber + 2) * 2 - 1] = BallVerticesBufferData[3];



    //cross
    BallVerticesBufferData[(verticesNumber + 2) * 2 ] = 0.0f;
    BallVerticesBufferData[(verticesNumber + 2) * 2 + 1] = 1.0f;

    BallVerticesBufferData[(verticesNumber + 2) * 2 + 2] = 0.0f;
    BallVerticesBufferData[(verticesNumber + 2) * 2 + 3] = -1.0f;

    BallVerticesBufferData[(verticesNumber + 2) * 2 + 4] = -1.0f;
    BallVerticesBufferData[(verticesNumber + 2) * 2 + 5] = 0.0f;

    BallVerticesBufferData[(verticesNumber + 2) * 2 + 6] = 1.0f;
    BallVerticesBufferData[(verticesNumber + 2) * 2 + 7] = 0.0f;



    glBufferData(GL_ARRAY_BUFFER, sizeof(BallVerticesBufferData), BallVerticesBufferData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, BallVerticesVB);
    glVertexAttribPointer(
                0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                2,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );

    glDisableVertexAttribArray(0);

    glGenBuffers(1, &BallColorVB);
    glBindBuffer(GL_ARRAY_BUFFER, BallColorVB);

    GLfloat BallColorBufferData[(verticesNumber + 2+4) * 3];

    BallColorBufferData[0] = 1.0f;
    BallColorBufferData[1] = 1.0f;
    BallColorBufferData[2] = 1.0f;

    for(int i = 0; i < verticesNumber+5; i++)
    {
        BallColorBufferData[3*i + 3] = 1.0f;
        BallColorBufferData[3*i + 4] = 1.0f;
        BallColorBufferData[3*i + 5] = 1.0f;
    }


    glBufferData(GL_ARRAY_BUFFER, sizeof(BallColorBufferData), BallColorBufferData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, BallColorVB);
    glVertexAttribPointer(
        1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
    );

    glDisableVertexAttribArray(1);
}


Ball::~Ball()
{
}

void Ball::clean()
{
    glDeleteBuffers(1, &BallVerticesVB);
    glDeleteBuffers(1, &BallColorVB);

    glDeleteVertexArrays(1, &BallVA);
    glDeleteProgram(BallShader);
}

void Ball::draw()
{
    glBindVertexArray(BallVA);

    glUseProgram(BallShader);

    GLint uniLine = glGetUniformLocation(BallShader, "isLine");
    GLint uniScale = glGetUniformLocation(BallShader, "scaling");
    GLint uniTrans = glGetUniformLocation(BallShader, "translation");
    GLint uniAngle = glGetUniformLocation(BallShader, "angle");

    vec2 trans = vec2(0,0) + center;

    glUniform2f(uniTrans, trans.x, trans.y);
    glUniform2f(uniScale, scale.y, scale.y);
    glUniform1f(uniAngle, angle);

    glEnableVertexAttribArray(0);   //vertices
    glEnableVertexAttribArray(1);   //colors

    glUniform1i(uniLine, false);

    glDrawArrays(GL_TRIANGLE_FAN, 0, verticesNumber+2);

    glUniform1i(uniLine, true);

    glDrawArrays(GL_LINES, verticesNumber+2, 2);
    glDrawArrays(GL_LINES, verticesNumber+4, 2);

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}

void Ball::move(double frameTime)
{
    center += vec2(speed.x * frameTime, speed.y * frameTime);
}

void Ball::rotate(double frameTime)
{
    angle += angleSpeed * frameTime;
}

void Ball::drawLifes(int lifesNumber)
{
    glBindVertexArray(BallVA);

    glUseProgram(BallShader);

    GLint uniLine = glGetUniformLocation(BallShader, "isLine");
    GLint uniScale = glGetUniformLocation(BallShader, "scaling");
    GLint uniTrans = glGetUniformLocation(BallShader, "translation");
    GLint uniAngle = glGetUniformLocation(BallShader, "angle");

    for(int i=0; i<lifesNumber; i++)
    {
        vec2 translation = vec2(-0.95f, -0.95f + 3*radius*i);

        float lifeAngle = 0;

        glUniform2f(uniTrans, translation.x, translation.y);
        glUniform2f(uniScale, scale.y, scale.y);
        glUniform1f(uniAngle, lifeAngle);

        glEnableVertexAttribArray(0);   //vertices
        glEnableVertexAttribArray(1);   //colors

        glUniform1i(uniLine, false);

        glDrawArrays(GL_TRIANGLE_FAN, 0, verticesNumber+2);

        glUniform1i(uniLine, true);

        glDrawArrays(GL_LINES, verticesNumber+2, 2);
        glDrawArrays(GL_LINES, verticesNumber+4, 2);

    }
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}

void Ball::startMoving()
{
    float randX =  (float)rand() / (float)RAND_MAX;
    //randX is between [0, 1.0]

    int randPlusX = rand() % 2;

    speed = vec2(0,1.0f);

    if(randPlusX==0)
    {
        speed -= vec2(randX * 0.25f, 0);
    }
    else
    {
        speed += vec2(randX * 0.25f, 0);
    }
    speed = normalize(speed);

}
