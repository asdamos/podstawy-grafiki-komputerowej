#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec2 position;
layout(location = 1) in vec3 color;

uniform vec2 translation;
uniform vec2 scaling;
uniform bool isLine;

out vec3 Color;

void main()
{
    if(isLine)
    {
        Color = vec3(0.0f, 0.0f, 0.0f);
    }
    else
    {
        Color = color;
    }

    gl_Position = vec4(position.x * scaling.x + translation.x, position.y * scaling.y + translation.y, 0.0, 1.0);


}

