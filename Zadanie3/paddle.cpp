#include "paddle.h"

#include <cstdio>

#define PI 3.14159265


Paddle::Paddle()
{
}

void Paddle::init()
{
    height = 0.02f;
    width = 0.30f;

    //because we start paddle as rectangle
    scale = vec2(width / 2, height / 2);

    center = vec2(0.0f, -0.9f + height/2);

    speed = vec2(0.60f, 0);

    colision = 0; //1 - left, 2 - right

    //if we define dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx)
    paddleNormals[0] = normalize(vec2(0, width));   //top
    paddleNormals[1] = normalize(vec2(height , 0));   //right
    paddleNormals[2] = normalize(vec2(0 , -width));   //bottom
    paddleNormals[3] = normalize(vec2(-height , 0));   //left


    float frameA = 1.8;
    float frameB = 1.8;
    float frameR = frameA / 2.0;

    float frameS = frameR / cos(30 * PI / 180.0);
    float frameH = (frameB - frameS) / 2;

    minX = -0.9f + frameH;  //down left
    maxX = 0.9f - frameH;   //down right

    PaddleShader = LoadShaders("paddleVS.glsl", "paddleFS.glsl" );

    glGenVertexArrays(1, &PaddleVA);
    glBindVertexArray(PaddleVA);

    glGenBuffers(1, &PaddleVerticesVB);
    glBindBuffer(GL_ARRAY_BUFFER, PaddleVerticesVB);

    GLfloat PaddleVerticesBufferData[] = {
        0.0f, 0.0f,     //center
        1.0f, 1.0f,     //up right
        1.0f, -1.0f,    //down right
        -1.0f, -1.0f,   //down left
        -1.0f, 1.0f,    //up left
        1.0f, 1.0f      //up right
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof(PaddleVerticesBufferData), PaddleVerticesBufferData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, PaddleVerticesVB);
    glVertexAttribPointer(
                0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                2,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );

    glDisableVertexAttribArray(0);

    glGenBuffers(1, &PaddleColorVB);
    glBindBuffer(GL_ARRAY_BUFFER, PaddleColorVB);

    GLfloat PaddleColorBufferData[] = {
        0.85f, 0.85f, 0.85f,   //white
        0.8f, 0.8f, 0.8f,      //gray
        0.8f, 0.8f, 0.8f,      //gray
        0.8f, 0.8f, 0.8f,      //gray
        0.8f, 0.8f, 0.8f,      //gray
        0.8f, 0.8f, 0.8f       //gray
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof(PaddleColorBufferData), PaddleColorBufferData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, PaddleColorVB);
    glVertexAttribPointer(
        1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
    );

    glDisableVertexAttribArray(1);
}


Paddle::~Paddle()
{

}

void Paddle::clean()
{
    glDeleteBuffers(1, &PaddleVerticesVB);
    glDeleteBuffers(1, &PaddleColorVB);

    glDeleteVertexArrays(1, &PaddleVA);
    glDeleteProgram(PaddleShader);
}

void Paddle::draw()
{
    glBindVertexArray(PaddleVA);

    glUseProgram(PaddleShader);

    glEnableVertexAttribArray(0);   //vertices
    glEnableVertexAttribArray(1);   //colors

    GLint uniScale = glGetUniformLocation(PaddleShader, "scaling");
    GLint uniLine = glGetUniformLocation(PaddleShader, "isLine");
    GLint uniTrans = glGetUniformLocation(PaddleShader, "translation");

    vec2 trans = vec2(0.0f, 0.0f) + center;

    glUniform2f(uniTrans, trans.x, trans.y);

    glUniform2f(uniScale, scale.x, scale.y);
    glUniform1i(uniLine, false);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 6);

    glUniform1i(uniLine, true);


    glDrawArrays(GL_LINE_LOOP, 1, 4);

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}

void Paddle::moveLeft(double frameTime)
{
    center -= vec2(speed.x * (float)(frameTime), speed.y);

    if(center.x - width/2 < minX)
    {
        center.x = minX + width/2;
    }
}

void Paddle::moveRight(double frameTime)
{
    center += vec2(speed.x * (float)(frameTime), speed.y);

    if(center.x + width/2 > maxX)
    {
        center.x = maxX - width/2;
    }
}

std::vector<vec2> Paddle::checkColisions(Ball ball_obj)
{
    std::vector<vec2> reflections;

    colision = 0;

    vec2 upRight = vec2(center.x + width/2, center.y + height/2);
    vec2 upLeft = vec2(center.x - width/2, center.y + height/2);
    vec2 bottomRight = vec2(center.x + width/2, center.y - height/2);
    vec2 bottomLeft = vec2(center.x - width/2, center.y - height/2);

    if(colisionBetweenSectionAndBall(upLeft, upRight, ball_obj))
    {
        //printf("gora\n");
        vec2 aditionalVector = paddleNormals[0];

        if(ball_obj.center.x < center.x)
        {
            aditionalVector.x -= 0.05f;
        }
        else
        {
            aditionalVector.x += 0.05f;
        }


        reflections.push_back(normalize(aditionalVector));
    }

    if(colisionBetweenSectionAndBall(upRight, bottomRight, ball_obj))
    {
        colision = 2;
        //printf("prawo\n");
        reflections.push_back(normalize(paddleNormals[1]));
    }

    if(colisionBetweenSectionAndBall(bottomRight, bottomLeft, ball_obj))
    {

        //printf("dol\n");
        reflections.push_back(normalize(paddleNormals[2]));
    }

    if(colisionBetweenSectionAndBall(bottomLeft, upLeft, ball_obj))
    {
        colision = 1;
        //printf("lewo\n");
        reflections.push_back(normalize(paddleNormals[3]));
    }
    return reflections;

}

