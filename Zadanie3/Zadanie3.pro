+TEMPLATE = app

+CONFIG += console c++11

+CONFIG -= app_bundle

+CONFIG -= qt

+

+SOURCES += main.cpp \

+    game.cpp \

+    shader.cpp

+

+HEADERS += \

+    game.h \

+    shader.hpp

HEADERS += \
    shader.hpp \
    game.h \
    frame.h \
    background.h \
    paddle.h \
    ball.h \
    brickscontainer.h \
    brick.h \
    functions.h

SOURCES += \
    game.cpp \
    main.cpp \
    shader.cpp \
    frame.cpp \
    background.cpp \
    paddle.cpp \
    ball.cpp \
    brickscontainer.cpp \
    brick.cpp \
    functions.cpp

DISTFILES += \
    HexFrameFS.glsl \
    HexFrameVS.glsl \
    BackgroundFS.glsl \
    BackgroundVS.glsl \
    paddleVS.glsl \
    paddleFS.glsl \
    ballFS.glsl \
    ballVS.glsl \
    brickVS.glsl \
    brickFS.glsl
