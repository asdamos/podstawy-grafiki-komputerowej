#ifndef GAME_H
#define GAME_H

#include <GL/glew.h>
#include <sys/time.h>
// Include GLFW
#include <GLFW/glfw3.h>
#include <errno.h>
#include <glm/glm.hpp>
#include <cstdio>

#include "frame.h"
#include "background.h"
#include "paddle.h"
#include "ball.h"
#include "brickscontainer.h"

class Game
{
public:
    Game();

    //Initializers
    bool initGLFW();
    bool createWindow();
    bool initGLEW();

    void initGame();

    //hexagonal frame
    Frame frame_obj;

    Background bg_obj;
    Paddle paddle_obj;

    BricksContainer bricks_obj;

    Ball ball_obj;

    void checkCollisions();

    void loop();
    void clean();

    GLuint vertexbuffer;
    GLuint VertexArrayID;

    bool leftKeyReleased;
    bool rightKeyReleased;
    bool spaceKeyReleased;
    bool upKeyReleased;

    bool pause;
    bool ballMoves;

    bool gameOver;

    int lifesNumber;


    GLFWwindow * window;

    void handleInput(double frameTime);

    void ballOnPaddle();

};

#endif // GAME_H
