// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <ctime>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
using namespace glm;

#include "shader.hpp"
#include "game.h"

int main(int argc, char * argv[])
{

    Game memory;

    std::srand(time(NULL));

    if(!memory.initGLFW())
    {
        return -1;
    }

    if(!memory.createWindow())
    {
        return -1;
    }

    if(!memory.initGLEW())
    {
        return -1;
    }

    memory.initGame();

    printf("\n");
    printf("Obsługa gry:\n");
    printf("Strzałka w lewo/prawo - ruch paletki lewo/prawo\n");
    printf("Strzałka w gore - wybicie piłki\n");
    printf("Spacja - pauza\n");
    printf("Escape - wyjście\n");

    memory.loop();

    memory.clean();

    return 0;
}
