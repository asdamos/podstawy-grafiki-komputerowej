#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <cmath>

#include <glm/glm.hpp>

using namespace glm;

#include "ball.h"
#include <vector>

bool colisionBetweenSectionAndBall(vec2 P1, vec2 P2, Ball ball);

float distance(vec2 P1, vec2 P2, vec2 center);

#endif // FUNCTIONS_H
