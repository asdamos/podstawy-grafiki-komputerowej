#ifndef LIGHT_H
#define LIGHT_H

#include <glm/glm.hpp>


class Light
{
public:
    Light();
    Light(glm::vec3 position, glm::vec3 color, float ambientCoefficient, float attenuation, bool isDirectional);

    void setLight(glm::vec3 position, glm::vec3 color, float ambientCoefficient, float attenuation);

    glm::vec4 position;
    glm::vec3 color;
    float ambientCoefficient;
    float attenuation;

    float coneAngle;
    glm::vec3 coneDirection;


    bool isDirectional;

    //getters and setters
    glm::vec4 getPosition() const;
    void setPosition(const glm::vec4 &value);


    void setPosition(glm::vec3 position, bool isDirectional);


    glm::vec3 getColor() const;
    void setColor(const glm::vec3 &value);

    float getAmbientCoefficient() const;
    void setAmbientCoefficient(float value);

    float getAttenuation() const;
    void setAttenuation(float value);

    glm::vec3 getConeDirection() const;
    void setConeDirection(const glm::vec3 &value);
};

#endif // LIGHT_H
