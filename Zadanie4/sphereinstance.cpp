#include "sphereinstance.h"
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include <glm/gtc/type_ptr.hpp>

#include <cstdio>
SphereInstance::SphereInstance()
{

}

SphereInstance::SphereInstance(SphereAsset *asset, vec3 position, vec3 color, float radius)
{
    this->asset = asset;
    this->position = position;
    this->color = color;
    this->radius = radius;
}

void SphereInstance::draw(Camera cam, std::vector<Light> lights, bool isBonus)
{
    glBindVertexArray(asset->VAO);

    glUseProgram(asset->Shader);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glUniform1f(asset->alphaUniform, 1.0f);

    glUniform3f(asset->colorUniform, color.x, color.y, color.z);

    int lightsNumber = lights.size();

    glUniform1i(asset->LightsNumberUniform, lightsNumber);
    for(int i=0; i<lightsNumber; i++)
    {
        glUniform4f(asset->LightPositionUniform[i], lights[i].position.x, lights[i].position.y, lights[i].position.z, lights[i].position.w);
        glUniform3f(asset->LightColorUniform[i], lights[i].color.x, lights[i].color.y, lights[i].color.z);
        glUniform1f(asset->LightAmbientCoefficientUniform[i], lights[i].ambientCoefficient);
        glUniform1f(asset->LightAttenuationUniform[i], lights[i].attenuation);
        glUniform1f(asset->ConeAngleUniform[i], lights[i].coneAngle);
        glUniform3f(asset->ConeDirectionUniform[i], lights[i].coneDirection.x, lights[i].coneDirection.y, lights[i].coneDirection.z);
    }




    glUniform1f(asset->MaterialShininessUniform, asset->shinines);
    glUniform3f(asset->MaterialSpecularColorUniform, asset->specularColor.x, asset->specularColor.y, asset->specularColor.z);

    if(isBonus)
    {
        glUniform1i(asset->IsBonusBubbleUniform, 1);
    }
    else
    {
        glUniform1i(asset->IsBonusBubbleUniform, 0);
    }


    glm::mat4 ProjectionMatrix = cam.getProjectionMatrix();
    glm::mat4 ViewMatrix = cam.getViewMatrix();


    glUniform3f(asset->CameraPositionUniform, cam.position.x, cam.position.y, cam.position.z);


    glm::mat4 ModelMatrix = glm::mat4(1);

    ModelMatrix = glm::translate(ModelMatrix, position);
    ModelMatrix = glm::scale(ModelMatrix, glm::vec3(radius, radius, radius));

    glUniformMatrix4fv(asset->ModelMatrixUniform, 1, GL_FALSE, &ModelMatrix[0][0]);
    glUniformMatrix4fv(asset->ViewMatrixUniform, 1, GL_FALSE, &ViewMatrix[0][0]);
    glUniformMatrix4fv(asset->ProjectionMatrixUniform, 1, GL_FALSE, &ProjectionMatrix[0][0]);


    glDrawArrays(asset->drawType, asset->drawStart, asset->drawCount);

    if(isBonus)
    {
        glUniform1i(asset->IsBonusBubbleUniform, 0);
        glDrawArrays(asset->drawType, asset->drawStart, asset->drawCount);
    }


    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}
