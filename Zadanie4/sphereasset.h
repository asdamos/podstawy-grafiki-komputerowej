#ifndef SPHEREASSET_H
#define SPHEREASSET_H

#include "modelasset.h"

class SphereAsset : public ModelAsset
{
public:
    SphereAsset(int slicesNumber, int vertexesPerSlice);

    int slicesNumber;
    int vertexesPerSlice;

    void init();
    void clean();

};

#endif // SPHEREASSET_H
