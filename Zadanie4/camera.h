#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


class Camera
{
public:
    Camera();

    void setResolution(glm::ivec2 resolution);
    void setPostion(glm::vec3 position);
    void setHorizontalAngle(float angle);
    void setVerticalAngle(float angle);
    void setFoV(float FoV, float min, float max);

    void computeNewOrientation(double xpos, double ypos, float deltaTime);

    void changeFoV(double yoffset);

    void moveForward(float deltaTime, glm::vec3 aquariumSize);
    void moveBackward(float deltaTime, glm::vec3 aquariumSize);
    void moveLeft(float deltaTime, glm::vec3 aquariumSize);
    void moveRight(float deltaTime, glm::vec3 aquariumSize);
    void moveUp(float deltaTime, glm::vec3 aquariumSize);
    void moveDown(float deltaTime, glm::vec3 aquariumSize);

    glm::ivec2 resolution;

    glm::vec3 position;

    float horizontalAngle;
    float verticalAngle;
    float FoV;
    float minFoV, maxFoV;

    float near, far;

    float mouseSpeed;
    float movingSpeed;

    float FoVAngleSpeed;

    glm::mat4 ProjectionMatrix;
    glm::mat4 ViewMatrix;
    glm::mat4 ModelMatrix;
    glm::mat4 MVP;

    glm::vec3 getDirection();
    glm::vec3 getRight();
    glm::vec3 getUp();


    bool isArcBall;
    float radius;
    glm::vec3 arcBallPosition;


    glm::mat4 getViewMatrix();

    glm::mat4 getProjectionMatrix();
};

#endif // CAMERA_H
