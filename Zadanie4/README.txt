Śmiercionośne bąbelki
Zadanie 4 na zajęcia z Podstaw Grafiki Komputerowej
Autor: Adam Sawicki
Indeks: 270814

Sterowanie:
Ruch myszą 	- rozglądanie się
A 		- ruch w lewo
D 		- ruch w prawo
W		- ruch do przodu
S		- ruch do tyłu
Lewy Ctrl	- ruch w dół
Spacja		- ruch do góry
Tab		- zmiana kamery z zewnętrznej na kamerę gracza
C		- zmiana przełącznie między kamerą pierwszo i trzecioosobową, opcja działa tylko w trybie kamery gracza
Rolka myszy	- zmiana Field of View


O grze:
Zadaniem gracza jest dopłynięcie na drugi koniec akwarium. Gracz zostanie wtedy przeniesiony na początek nowego poziomu.
Za każdego poziomu gracz dostaje pewną liczbę punktów.
Gracz ma początkowo pewną ilość żyć. Gracz może stracić życie poprzez wlecenie w zwykły bąbelek, ale może również zdobyć życie oraz punkty po wleceniu w świecący bąbelek bonusowy.

Po utracie wszystkich żyć następuje koniec gry, gracz nie może się już poruszać a w terminalu następuje wypisanie zdobytej ilości punktów.


Udanej rozgrywki
