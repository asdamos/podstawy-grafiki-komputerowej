#include "planeinstance.h"
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include <glm/gtc/type_ptr.hpp>
#include <cstdio>

PlaneInstance::PlaneInstance(PlaneAsset *asset, glm::vec3 position, glm::vec3 color, glm::vec3 size, glm::vec3 rotation, float alpha, float rotationAngle)
{
    this->asset = asset;
    this->position = position;
    this->color = color;
    this->size = size;
    this->alpha = alpha;
    ModelMatrix = glm::mat4(1.0f);



    ModelMatrix = glm::translate(ModelMatrix, position);
    ModelMatrix = glm::scale(ModelMatrix, size);

    ModelMatrix = glm::rotate(ModelMatrix, glm::radians(rotationAngle), rotation);

}

void PlaneInstance::draw(Camera cam, std::vector<Light> lights)
{
    glBindVertexArray(asset->VAO);

    glUseProgram(asset->Shader);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);


    glUniform1f(asset->alphaUniform, alpha);

    glUniform3f(asset->colorUniform, color.x, color.y, color.z);

    int lightsNumber = lights.size();

    glUniform1i(asset->LightsNumberUniform, lightsNumber);

    for(int i=0; i<lightsNumber; i++)
    {
        glUniform4f(asset->LightPositionUniform[i], lights[i].position.x, lights[i].position.y, lights[i].position.z, lights[i].position.w);

        glUniform3f(asset->LightColorUniform[i], lights[i].color.x, lights[i].color.y, lights[i].color.z);
        glUniform1f(asset->LightAmbientCoefficientUniform[i], lights[i].ambientCoefficient);
        glUniform1f(asset->LightAttenuationUniform[i], lights[i].attenuation);
        glUniform1f(asset->ConeAngleUniform[i], lights[i].coneAngle);
        glUniform3f(asset->ConeDirectionUniform[i], lights[i].coneDirection.x, lights[i].coneDirection.y, lights[i].coneDirection.z);
    }

    glUniform3f(asset->CameraPositionUniform, cam.position.x, cam.position.y, cam.position.z);

    glUniform1f(asset->MaterialShininessUniform, asset->shinines);
    glUniform3f(asset->MaterialSpecularColorUniform, color.x, color.y, color.z);

    //glUniform3f(asset->MaterialSpecularColorUniform, asset->specularColor.x, asset->specularColor.y, asset->specularColor.z);

    glUniform1i(asset->IsBonusBubbleUniform, 0);

    glm::mat4 ProjectionMatrix = cam.getProjectionMatrix();
    glm::mat4 ViewMatrix = cam.getViewMatrix();


    glUniformMatrix4fv(asset->ModelMatrixUniform, 1, GL_FALSE, &ModelMatrix[0][0]);
    glUniformMatrix4fv(asset->ViewMatrixUniform, 1, GL_FALSE, &ViewMatrix[0][0]);
    glUniformMatrix4fv(asset->ProjectionMatrixUniform, 1, GL_FALSE, &ProjectionMatrix[0][0]);


    glDrawArrays(asset->drawType, asset->drawStart, asset->drawCount);

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}


