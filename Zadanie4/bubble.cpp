#include "bubble.h"
#include <cstdlib>
#include <ctime>
#include <cstdio>

Bubble::Bubble()
{
}

void Bubble::draw(Camera cam, std::vector<Light> lights)
{
    sphere.draw(cam, lights, hasLight);
}

void Bubble::generateRandomParametres(vec3 aquarium, float playerRadius, bool hasLight, bool isStartingFromloor)
{
    float random = float(rand()) / float(RAND_MAX);

    //radius 0.5 - 1.0
    random *= 0.5f;
    random +=0.5f;

    radius = random;



    //position

    if(isStartingFromloor)
    {
        position.y = -aquarium.y - radius;
    }
    else
    {
        random = float(rand()) / float(RAND_MAX);
        random *= aquarium.y * 2;
        random += -aquarium.y - radius;
        position.y = random;
    }


    random = float(rand()) / float(RAND_MAX);
    random *= (aquarium.x * 2 - 2 * radius);
    random += -aquarium.x + radius;
    position.x = random;

    //safezone for player
    random = float(rand()) / float(RAND_MAX);
    random *= (aquarium.z * 2 - 2*radius - 4*playerRadius);
    random += -aquarium.z + radius;
    position.z = random;

    //color
    color.x = float(rand()) / float(RAND_MAX);
    color.y = float(rand()) / float(RAND_MAX);
    color.z = float(rand()) / float(RAND_MAX);

    //speed 1-5
    random = float(rand()) / float(RAND_MAX);
    random *= 4.0f;
    random +=1.0f;
    speed = random;

    this->hasLight = hasLight;

    light.setPosition(position, false);
    light.color = glm::vec3(0.95,0.95,0.95); //strong white light
//    light.color = color;
    light.attenuation = 0.3f;
    light.ambientCoefficient = 0.0f; //no ambient light
    light.coneAngle = 180.0f;
    light.coneDirection = glm::vec3(0,0,1);

}

void Bubble::generateSphere(SphereAsset *asset)
{
    sphere = SphereInstance(asset, this->position, color, radius);
}

void Bubble::update(float deltaTime)
{
    position.y += speed * deltaTime;
    sphere.position.y = position.y;

    if(hasLight)
    {
        light.position.x = position.x;
        light.position.y = position.y;
        light.position.z = position.z;
    }
}

void Bubble::updatePosition(vec3 newPosition)
{
    this->position = newPosition;
    sphere.position = newPosition;
}

bool Bubble::passedCeiling(vec3 aquarium)
{
    if(position.y >= aquarium.y - radius)
    {
        return true;
    }
    return false;
}

bool Bubble::isColliding(Bubble b)
{
    float distance = glm::distance(this->position, b.position);
    if(distance < (this->radius + b.radius))
    {
        return true;
    }
    return false;
}

bool Bubble::isCollidingWithLevelEnd(vec3 aquarium)
{
    float distance = std::abs(-aquarium.z - this->position.z);
    if(distance < this->radius)
    {
        return true;
    }
    return false;
}


void Bubble::moveForward(float deltaTime, glm::vec3 aquariumSize, Camera cam)
{
    glm::vec3 direction = cam.getDirection();

    glm::vec3 newPosition = position + direction * deltaTime * speed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

void Bubble::moveBackward(float deltaTime, glm::vec3 aquariumSize, Camera cam)
{
    glm::vec3 direction = cam.getDirection();

    glm::vec3 newPosition = position - direction * deltaTime * speed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

void Bubble::moveLeft(float deltaTime, glm::vec3 aquariumSize, Camera cam)
{
    glm::vec3 right = cam.getRight();

    glm::vec3 newPosition = position - right * deltaTime * speed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

void Bubble::moveRight(float deltaTime, glm::vec3 aquariumSize, Camera cam)
{
    glm::vec3 right = cam.getRight();

    glm::vec3 newPosition = position + right * deltaTime * speed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

void Bubble::moveUp(float deltaTime, glm::vec3 aquariumSize, Camera cam)
{
    glm::vec3 up = cam.getUp();

    glm::vec3 newPosition = position + up * deltaTime * speed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

void Bubble::moveDown(float deltaTime, glm::vec3 aquariumSize, Camera cam)
{
    glm::vec3 up = cam.getUp();

    glm::vec3 newPosition = position - up * deltaTime * speed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

