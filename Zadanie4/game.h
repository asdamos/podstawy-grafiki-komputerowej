#ifndef GAME_H
#define GAME_H

#include <GL/glew.h>
#include <sys/time.h>
// Include GLFW
#include <GLFW/glfw3.h>
#include <errno.h>
#include <glm/glm.hpp>
#include <cstdio>

#include "camera.h"

#include "sphereasset.h"
#include "sphereinstance.h"

#include "planeasset.h"
#include "planeinstance.h"
#include "light.h"

#include <vector>
#include "bubble.h"

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

class Game
{
public:
    Game();

    //Initializers
    bool initGLFW();
    bool createWindow();
    bool initGLEW();

    void initGame();

    void initPlayerBubble();
    void initPlanes();


    void loop();
    void clean();

    void initPlayerCamera();
    void initOutdoorCamera();

    void getMouseEvents(float deltaTime);
    void getKeyboardEvents(float deltaTime);


    GLFWwindow * window;

    Camera playerCamera;
    Camera outdoorCamera;

    Camera * activeCamera;

    bool firstPerson = true;

    bool playerCameraActive;

    bool wKeyReleased;
    bool sKeyReleased;
    bool aKeyReleased;
    bool dKeyReleased;
    bool ctrlKeyReleased;
    bool spaceKeyReleased;
    bool tabKeyWasPressed;
    bool cKeyWasPressed;

    bool gameOver = false;

    void switchCameras();


    void checkWKey(float deltaTime);
    void checkSKey(float deltaTime);
    void checkAKey(float deltaTime);
    void checkDKey(float deltaTime);
    void checkCtrlKey(float deltaTime);
    void checkSpaceKey(float deltaTime);

    void checkCKey();
    void checkTabKey();


    void generateRemainingBubbles(bool isInitialGeneration);

    //parametres
    int maxBubbles = 50;
    glm::ivec2 resolution;
    glm::vec3 aquariumSize;
    float playerSpeed;
    int lifes = 10;
    int points = 0;
    float bubblesLevelCoefficient = 1.1f;

    int lightsNumber = 2;   //sun and player

    //models
    SphereAsset sphereAssetObj = SphereAsset(36, 36);
    PlaneAsset planeAssetObj;

    //instances
    std::vector<Bubble> bubbles;
    std::vector<PlaneInstance> planes;


    Bubble playerBubble;


    //lights
    std::vector<Light> lights;

    Light playerLight;
    Light sunLight;

};

#endif // GAME_H
