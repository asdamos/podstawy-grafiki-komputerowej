#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
uniform vec3 color;


uniform bool isBonusBubble;


// Values that stay constant for the whole mesh.

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;


uniform float alpha;

out vec3 fragmentVertex;
out vec3 fragmentNormal;
out vec4 fragmentColor;



void main(){

    fragmentVertex = vertex;
    if(isBonusBubble)
    {
        fragmentNormal = normal * -1;
    }
    else
    {
        fragmentNormal = normal;
    }

    fragmentColor.rgb = color;
    fragmentColor.a = alpha;


    gl_Position =  ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(vertex,1);
}
