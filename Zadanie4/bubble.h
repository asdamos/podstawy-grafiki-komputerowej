#ifndef BUBBLE_H
#define BUBBLE_H
#include <glm/glm.hpp>
#include "sphereinstance.h"
#include "light.h"
#include "camera.h"

class Bubble
{
public:
    Bubble();

    void generate();

    float radius;
    glm::vec3 position;
    float speed;

    glm::vec3 color;

    SphereInstance sphere;

    bool hasLight;

    Light light;

    void draw(Camera cam, std::vector<Light> lights);

    void generateRandomParametres(glm::vec3 aquarium, float playerRadius, bool hasLight, bool isStartingFromloor);
    void generateSphere(SphereAsset *asset);

    void update(float deltaTime);
    void updatePosition(glm::vec3 newPosition);

    bool passedCeiling(glm::vec3 aquarium);

    bool isColliding(Bubble b);

    bool isCollidingWithLevelEnd(glm::vec3 aquarium);

    void moveForward(float deltaTime, glm::vec3 aquariumSize, Camera cam);
    void moveBackward(float deltaTime, glm::vec3 aquariumSize, Camera cam);
    void moveLeft(float deltaTime, glm::vec3 aquariumSize, Camera cam);
    void moveRight(float deltaTime, glm::vec3 aquariumSize, Camera cam);
    void moveUp(float deltaTime, glm::vec3 aquariumSize, Camera cam);
    void moveDown(float deltaTime, glm::vec3 aquariumSize, Camera cam);

};

#endif // BUBBLE_H
