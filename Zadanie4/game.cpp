#include "game.h"

#include <GL/glew.h>
#include <ctime>
#include <cmath>
// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "shader.h"
#include <cstdlib>
#include <unistd.h>
double scrollOffset;


Game::Game()
{
    resolution = glm::ivec2(1024, 768);
    aquariumSize = glm::vec3(10, 10, 20);
    wKeyReleased = true;
    sKeyReleased = true;
    aKeyReleased = true;
    dKeyReleased = true;
    ctrlKeyReleased = true;
    spaceKeyReleased = true;

    tabKeyWasPressed = false;
    cKeyWasPressed = false;
    playerCameraActive = true;
}

void Game::initGame()
{
    sphereAssetObj.init();
    planeAssetObj.init();

    sunLight.setPosition(glm::vec4(10,100,10,0));
    sunLight.color = glm::vec3(1,1,0.8); //weak yellowish light
    sunLight.ambientCoefficient = 0.06;

    initPlanes();
    initPlayerBubble();
    initPlayerCamera();
    initOutdoorCamera();
    activeCamera = & playerCamera;

    generateRemainingBubbles(true);

}

void Game::initPlayerBubble()
{
    playerSpeed = 15.0f;
    playerBubble.radius = 1.0f;
    playerBubble.position = glm::vec3(0.0f, -aquariumSize.y + playerBubble.radius, aquariumSize.z - playerBubble.radius);

    playerBubble.color = glm::vec3(1.0f, 0.0f, 0.0f);
    playerBubble.speed = playerSpeed;

    playerBubble.hasLight = false;
    playerBubble.sphere = SphereInstance(&sphereAssetObj, playerBubble.position,playerBubble.color, playerBubble.radius);
}

void Game::initPlanes()
{
    PlaneInstance left = PlaneInstance(&planeAssetObj,
                                       glm::vec3(-aquariumSize.x,0,0),
                                       glm::vec3(0.0f, 0.4f, 0.6f),
                                       aquariumSize,
                                       glm::vec3(0.0f, 0.0f, -1.0f),
                                       0.8f,
                                       90.0f);
    planes.push_back(left);

    PlaneInstance right = PlaneInstance(&planeAssetObj,
                                        glm::vec3(aquariumSize.x,0,0),
                                        glm::vec3(0.0f, 0.4f, 0.6f),
                                        aquariumSize,
                                        glm::vec3(0.0f, 0.0f, 1.0f),
                                        0.8f,
                                        90.0f);
    planes.push_back(right);


    PlaneInstance front = PlaneInstance(&planeAssetObj,
                                        glm::vec3(0,0,-aquariumSize.z),
                                        glm::vec3(1.0f,1.0f, 1.0f),
                                        aquariumSize,
                                        glm::vec3(1.0f, 0.0f, 0.0f),
                                        1.0f,
                                        90.0f);
    planes.push_back(front);


    PlaneInstance back = PlaneInstance(&planeAssetObj,
                                       glm::vec3(0,0,aquariumSize.z),
                                       glm::vec3(0.0f, 0.4f, 0.6f),
                                       aquariumSize,
                                       glm::vec3(-1.0f, 0.0f, 0.0f),
                                       0.8f,
                                       90.0f);
    planes.push_back(back);

    PlaneInstance aquariumFloor = PlaneInstance(&planeAssetObj,
                                                glm::vec3(0,-aquariumSize.y,0),
                                                glm::vec3(0.3f, 0.3f, 0.3f),
                                                aquariumSize,
                                                glm::vec3(1.0f, 0.0f, 0.0f),
                                                1.0f,
                                                0.0f);
    planes.push_back(aquariumFloor);

    PlaneInstance aquariumCeiling = PlaneInstance(&planeAssetObj,
                                                  glm::vec3(0,aquariumSize.y,0),
                                                  glm::vec3(0.0f, 0.7f, 1.0f),
                                                  aquariumSize,
                                                  glm::vec3(1.0f, 0.0f, 0.0f),
                                                  0.8f,
                                                  180.0f);
    planes.push_back(aquariumCeiling);
}

bool Game::initGLFW()
{
    // Initialise GLFW

    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        getchar();
        return false;
    }

    return true;
}

bool Game::createWindow()
{
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    this->window = glfwCreateWindow( resolution.x, resolution.y, "Adam Sawicki", NULL, NULL);
    glfwSetCursorPos(window, resolution.x / 2, resolution.y / 2);

    if( this->window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        getchar();
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(this->window);

    return true;
}

bool Game::initGLEW()
{
    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        return false;
    }

    return true;
}



void Game::loop()
{
    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    glfwSetScrollCallback(window, scroll_callback);

    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    glEnable(GL_CULL_FACE);


    //hack for setting cursor at startup
    glfwSwapBuffers(window);
    glfwPollEvents();
    glfwSetCursorPos(this->window, resolution.x / 2, resolution.y / 2);


    double lastTime = glfwGetTime();
    double startTime = glfwGetTime();

    int nbFrames = 0;
    do{

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        double currentTime = glfwGetTime();

        nbFrames++;
        if ( currentTime - startTime >= 1.0 ){ // If last prinf() was more than 1 sec ago
            // printf and reset timer
            //printf("%f ms/frame\n", 1000.0/double(nbFrames));
            nbFrames = 0;
            startTime += 1.0;
        }

        float deltaTime = float(currentTime - lastTime);
        lastTime = currentTime;

        if(!gameOver)
        {
            //update
            getMouseEvents(deltaTime);
            getKeyboardEvents(deltaTime);

            for(int i=0; i<bubbles.size(); i++)
            {
                bubbles[i].update(deltaTime);
            }


            //check colisions

            //player to level ending
            if(playerBubble.isCollidingWithLevelEnd(aquariumSize))
            {
                //new level

                bubbles.clear();
                lightsNumber = 2;   //only player and sun

                //set position of camera and player
                playerCamera.setPostion(glm::vec3(0.0f, -aquariumSize.y + playerBubble.radius, aquariumSize.z - playerBubble.radius));
                playerCamera.setHorizontalAngle(M_PI);
                playerCamera.setVerticalAngle(0.0f);

                playerBubble.position = playerCamera.position;

                points += maxBubbles;
                maxBubbles *= bubblesLevelCoefficient;

                generateRemainingBubbles(true);

            }
            else
            {
                //same level

                //player to bubbles
                for(int i=0; i<bubbles.size(); i++)
                {
                    if(playerBubble.isColliding(bubbles[i]))
                    {


                        if(bubbles[i].hasLight)
                        {
                            lightsNumber--;
                            points += maxBubbles * 0.1f;
                            lifes++;
                        }
                        else
                        {
                            lifes--;
                        }

                        bubbles.erase(bubbles.begin() + i);
                        i -= 1;
                    }
                }
                if(lifes==0)
                {
                    gameOver = true;
                    printf("GAME OVER\n");
                    printf("You earned %d points\n", points);
                }

                for(int i=0; i<bubbles.size(); i++)
                {
                    if(bubbles[i].passedCeiling(aquariumSize))
                    {
                        if(bubbles[i].hasLight)
                        {
                            lightsNumber--;
                        }

                        bubbles.erase(bubbles.begin() + i);
                        i -= 1;
                    }
                }

                generateRemainingBubbles(false);

            }

        }
        playerLight.setPosition(playerBubble.position, false);

        playerLight.color = glm::vec3(10,10,10); //strong red light
        playerLight.attenuation = 0.9f;
        playerLight.ambientCoefficient = 0.0f; //no ambient light
        playerLight.coneAngle = 30.0f;
        playerLight.coneDirection = playerCamera.getDirection();


        lights.push_back(sunLight);
        lights.push_back(playerLight);

        for(int i=0; i<bubbles.size();i++)
        {
            if(bubbles[i].hasLight)
            {
                lights.push_back(bubbles[i].light);
            }
        }



        //draw
        playerBubble.draw(*activeCamera,lights);
        for(int i=0; i<bubbles.size(); i++)
        {

            bubbles[i].draw(*activeCamera, lights);

        }

        for(int i=0; i<planes.size(); i++)
        {
            planes[i].draw(*activeCamera, lights);
        }

        lights.clear();
        //swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0);
}

void Game::clean()
{
    // Close OpenGL window and terminate GLFW

    bubbles.clear();
    planes.clear();

    sphereAssetObj.clean();
    planeAssetObj.clean();

    glfwTerminate();
}

void Game::initPlayerCamera()
{
    playerCamera.movingSpeed = playerSpeed;
    playerCamera.setResolution(this->resolution);
    playerCamera.setPostion(glm::vec3(0.0f, -aquariumSize.y + playerBubble.radius, aquariumSize.z - playerBubble.radius));
    playerCamera.setHorizontalAngle(M_PI);
    playerCamera.setVerticalAngle(0.0f);
    playerCamera.setFoV(45.0f, 20.0f, 150.0f);
    playerCamera.isArcBall = false;

}

void Game::initOutdoorCamera()
{
    outdoorCamera.setResolution(this->resolution);
    outdoorCamera.arcBallPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    outdoorCamera.setHorizontalAngle(1.57f);
    outdoorCamera.setVerticalAngle(-0.58f);
    outdoorCamera.setFoV(75.0f, 20.0f, 90.0f);
    outdoorCamera.isArcBall = true;
    outdoorCamera.radius = 40.0f;
}

void Game::getMouseEvents(float deltaTime)
{
    double xDouble, yDouble;

    glfwGetCursorPos(this->window, &xDouble, &yDouble);

    activeCamera->computeNewOrientation(xDouble, yDouble, deltaTime);

    if(scrollOffset != 0)
    {
        activeCamera->changeFoV(scrollOffset);
    }

    glfwSetCursorPos(this->window, resolution.x / 2, resolution.y / 2);

    scrollOffset = 0;
}

void Game::getKeyboardEvents(float deltaTime)
{

    checkWKey(deltaTime);
    checkSKey(deltaTime);
    checkAKey(deltaTime);
    checkDKey(deltaTime);
    checkCtrlKey(deltaTime);
    checkSpaceKey(deltaTime);


    //correct position is in bubble
    playerBubble.sphere.position = playerBubble.position;

    if(firstPerson)
    {
        playerCamera.position = playerBubble.position;
    }
    else
    {
        playerCamera.position = playerBubble.position - 15.0f * normalize(playerCamera.getDirection());
    }


    //switching first and third person view
    checkCKey();

    checkTabKey();
}

void Game::switchCameras()
{
    if(playerCameraActive)
    {
        playerCameraActive = false;
        activeCamera = &outdoorCamera;
    }
    else
    {
        playerCameraActive = true;
        activeCamera = &playerCamera;
    }
}

void Game::checkWKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS && sKeyReleased)
    {
        wKeyReleased = false;

        playerBubble.moveForward(deltaTime, aquariumSize, playerCamera);
    }
    else if(glfwGetKey(window, GLFW_KEY_W) == GLFW_RELEASE)
    {
        wKeyReleased = true;
    }
}

void Game::checkSKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS && wKeyReleased)
    {
        sKeyReleased = false;

        playerBubble.moveBackward(deltaTime, aquariumSize, playerCamera);
    }
    else if(glfwGetKey(window, GLFW_KEY_S) == GLFW_RELEASE)
    {
        sKeyReleased = true;
    }
}

void Game::checkAKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS && dKeyReleased)
    {
        aKeyReleased = false;

        playerBubble.moveLeft(deltaTime, aquariumSize, playerCamera);
    }
    else if(glfwGetKey(window, GLFW_KEY_A) == GLFW_RELEASE)
    {
        aKeyReleased = true;
    }
}

void Game::checkDKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS && aKeyReleased)
    {
        dKeyReleased = false;

        playerBubble.moveRight(deltaTime, aquariumSize, playerCamera);
    }
    else if(glfwGetKey(window, GLFW_KEY_D) == GLFW_RELEASE)
    {
        dKeyReleased = true;
    }
}

void Game::checkCtrlKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS && spaceKeyReleased)
    {
        ctrlKeyReleased = false;

        playerBubble.moveDown(deltaTime, aquariumSize, playerCamera);
    }
    else if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_RELEASE)
    {
        ctrlKeyReleased = true;
    }
}

void Game::checkSpaceKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && ctrlKeyReleased)
    {
        spaceKeyReleased = false;

        playerBubble.moveUp(deltaTime, aquariumSize, playerCamera);
    }
    else if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE)
    {
        spaceKeyReleased = true;
    }
}

void Game::checkCKey()
{
    if(glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
    {
        if(playerCameraActive)
        {
            cKeyWasPressed = true;
        }
    }
    else if(glfwGetKey(window, GLFW_KEY_C) == GLFW_RELEASE)
    {
        if(cKeyWasPressed && playerCameraActive)
        {
            firstPerson = !firstPerson;
            cKeyWasPressed = false;
        }
    }
}

void Game::checkTabKey()
{
    if(glfwGetKey(window, GLFW_KEY_TAB) == GLFW_PRESS)
    {
        tabKeyWasPressed = true;
    }
    else if(glfwGetKey(window, GLFW_KEY_TAB) == GLFW_RELEASE)
    {
        if(tabKeyWasPressed)
        {
            switchCameras();
            tabKeyWasPressed = false;
        }
    }
}

void Game::generateRemainingBubbles(bool isInitialGeneration)
{
    int remainingBubbles = maxBubbles - bubbles.size();

    int remainingLights = MAX_LIGHTS - lightsNumber;

    for(int i=0; i<remainingBubbles; i++)
    {
        Bubble b;

        if(remainingLights > 0)
        {
            b.generateRandomParametres(aquariumSize, playerBubble.radius, true, !isInitialGeneration);
            remainingLights--;
            lightsNumber++;
        }
        else
        {
            b.generateRandomParametres(aquariumSize, playerBubble.radius, false, !isInitialGeneration);
        }

        b.generateSphere(&sphereAssetObj);

        bubbles.push_back(b);
    }
}

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset)
{
    scrollOffset = yoffset;
}
