#include "light.h"

Light::Light()
{

}

Light::Light(glm::vec3 position, glm::vec3 color, float ambientCoefficient, float attenuation, bool isDirectional)
{
    if(isDirectional)
    {
        this->position = glm::vec4(position.x, position.y, position.z, 0.0f);
    }
    else
    {
        this->position = glm::vec4(position.x, position.y, position.z, 1.0f);
    }
    this->isDirectional = isDirectional;
    this->color = color;
    this->ambientCoefficient = ambientCoefficient;
    this->attenuation = attenuation;
}

void Light::setLight(glm::vec3 position, glm::vec3 color, float ambientCoefficient, float attenuation)
{
    this->position = glm::vec4(position.x, position.y, position.z, 1.0f);
    this->color = color;
    this->ambientCoefficient = ambientCoefficient;
    this->attenuation = attenuation;
}

glm::vec3 Light::getConeDirection() const
{
    return coneDirection;
}

void Light::setConeDirection(const glm::vec3 &value)
{
    coneDirection = value;
}

float Light::getAttenuation() const
{
    return attenuation;
}

void Light::setAttenuation(float value)
{
    attenuation = value;
}

float Light::getAmbientCoefficient() const
{
    return ambientCoefficient;
}

void Light::setAmbientCoefficient(float value)
{
    ambientCoefficient = value;
}

glm::vec3 Light::getColor() const
{
    return color;
}

void Light::setColor(const glm::vec3 &value)
{
    color = value;
}

glm::vec4 Light::getPosition() const
{
    return position;
}

void Light::setPosition(const glm::vec4 &value)
{
    position = value;
}

void Light::setPosition(glm::vec3 position, bool isDirectional)
{
    if(isDirectional)
    {
        this->position = glm::vec4(position.x, position.y, position.z, 0.0f);
    }
    else
    {
        this->position = glm::vec4(position.x, position.y, position.z, 1.0f);
    }
    this->isDirectional = isDirectional;
}
