#ifndef SPHEREINSTANCE_H
#define SPHEREINSTANCE_H
#include "sphereasset.h"
#include "camera.h"
#include "light.h"
#include <vector>

class SphereInstance
{
public:
    SphereInstance();
    SphereInstance(SphereAsset *asset, glm::vec3 position, glm::vec3 color, float radius);
    SphereAsset *asset;

    void draw(Camera cam, std::vector<Light> lights, bool isBonus);

    glm::vec3 position;
    glm::vec3 color;
    float radius;
};

#endif // SPHEREINSTANCE_H
