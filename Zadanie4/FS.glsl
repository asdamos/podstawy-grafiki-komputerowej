#version 330 core

#define MAX_LIGHTS 12

in vec4 fragmentColor;
in vec3 fragmentVertex;
in vec3 fragmentNormal;

uniform float materialShininess;
uniform vec3 materialSpecularColor;

uniform vec3 cameraPosition;

uniform mat4 ModelMatrix;

uniform int lightsNumber;

struct Light {
    vec4 position;
    vec3 color;
    float ambientCoefficient;
    float attenuation;
    //for spotlight
    float coneAngle;
    vec3 coneDirection;
};

uniform Light lights[MAX_LIGHTS];


out vec4 finalColor;

vec3 ApplyLight(Light light, vec4 surfaceColor, vec3 normal, vec3 surfacePosition, vec3 surfaceToCamera) {
    vec3 surfaceToLight;
    float attenuation = 1.0;
    if(light.position.w == 0.0)
    {
        //directional light
        surfaceToLight = normalize(light.position.xyz);
        attenuation = 1.0; //no attenuation for directional lights
    }
    else
    {
        //point light
        surfaceToLight = normalize(light.position.xyz - surfacePosition);
        float distanceToLight = length(light.position.xyz - surfacePosition);
        attenuation = 1.0 / (1.0 + light.attenuation * pow(distanceToLight, 2));

        //cone restrictions (affects attenuation)
        float lightToSurfaceAngle = degrees(acos(dot(-surfaceToLight, normalize(light.coneDirection))));
        if(lightToSurfaceAngle > light.coneAngle){
            attenuation = 0.0;
        }
    }

    //ambient
    vec3 ambient = light.ambientCoefficient * surfaceColor.rgb * light.color;

    //diffuse
    float diffuseCoefficient = max(0.0, dot(normal, surfaceToLight));
    vec3 diffuse = diffuseCoefficient * surfaceColor.rgb * light.color;

    //specular
    float specularCoefficient = 0.0;
    if(diffuseCoefficient > 0.0)
    {
        specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, normal))), materialShininess);
    }
    vec3 specular = specularCoefficient * materialSpecularColor * light.color;

    return ambient + attenuation*(diffuse + specular);
}


void main(){    

    vec4 surfaceColor = fragmentColor;
    mat3 normalMatrix = transpose(inverse(mat3(ModelMatrix)));
    vec3 normal = normalize(normalMatrix * fragmentNormal);
    vec3 surfacePosition = vec3(ModelMatrix * vec4(fragmentVertex, 1));
    vec3 surfaceToCamera = normalize(cameraPosition - surfacePosition);

    vec3 linearColor = vec3(0);

    for(int i=0; i<lightsNumber; i++)
    {
        linearColor += ApplyLight(lights[i], surfaceColor, normal, surfacePosition, surfaceToCamera);
    }

    //final color (after gamma correction)
    vec3 gamma = vec3(1.0/2.2);

    float surfaceToCameraLength = distance(surfacePosition, cameraPosition);

    float fogDensity = 0.05;

    vec4 fogColor = vec4(0.0, 0.5, 0.9, 1.0);
    float fogFactor = exp(-fogDensity*surfaceToCameraLength);
    fogFactor = 1.0-clamp(fogFactor, 0.0, 1.0);

    finalColor = vec4(pow(linearColor, gamma), surfaceColor.a);

    finalColor = mix(finalColor, fogColor, fogFactor);  //add fog
}
