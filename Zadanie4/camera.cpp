#include "camera.h"
#include <cmath>
#include <cstdio>
#define PI 3.14159265


Camera::Camera()
{
    mouseSpeed = 0.1f;
    FoVAngleSpeed = 1.0f;
    //movingSpeed = 15.0f;
    horizontalAngle = 0.0f;
    verticalAngle = 0.0f;

    near = 0.1f;
    far = 100.0f;
}

void Camera::setResolution(glm::ivec2 resolution)
{
    this->resolution = resolution;
}

void Camera::setPostion(glm::vec3 position)
{
    this->position = position;
}

void Camera::setHorizontalAngle(float angle)
{
    this->horizontalAngle = angle;
}

void Camera::setVerticalAngle(float angle)
{
    this->verticalAngle = angle;
}

void Camera::setFoV(float FoV, float min, float max)
{
    this->FoV = FoV;
    this->minFoV = min;
    this->maxFoV = max;
}

void Camera::computeNewOrientation(double xpos, double ypos, float deltaTime)
{
    //printf("%f %f\n", xpos, ypos);
    horizontalAngle += mouseSpeed * deltaTime * float(resolution.x/2 - xpos );
    verticalAngle   += mouseSpeed * deltaTime * float(resolution.y/2 - ypos );
}

void Camera::changeFoV(double yoffset)
{
    float newFoV = FoV + yoffset * FoVAngleSpeed;

    if(newFoV > maxFoV)
    {
        FoV = maxFoV;
    }
    else if(newFoV < minFoV)
    {
        FoV = minFoV;
    }
    else
    {
        FoV = newFoV;
    }
    //printf("fov %f\n", FoV);
}

void Camera::moveForward(float deltaTime, glm::vec3 aquariumSize)
{
    glm::vec3 direction = getDirection();

    glm::vec3 newPosition = position + direction * deltaTime * movingSpeed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

void Camera::moveBackward(float deltaTime, glm::vec3 aquariumSize)
{
    glm::vec3 direction = getDirection();

    glm::vec3 newPosition = position - direction * deltaTime * movingSpeed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

void Camera::moveLeft(float deltaTime, glm::vec3 aquariumSize)
{
    glm::vec3 right = getRight();

    glm::vec3 newPosition = position - right * deltaTime * movingSpeed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

void Camera::moveRight(float deltaTime, glm::vec3 aquariumSize)
{
    glm::vec3 right = getRight();

    glm::vec3 newPosition = position + right * deltaTime * movingSpeed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

void Camera::moveUp(float deltaTime, glm::vec3 aquariumSize)
{
    glm::vec3 up = getUp();

    glm::vec3 newPosition = position + up * deltaTime * movingSpeed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

void Camera::moveDown(float deltaTime, glm::vec3 aquariumSize)
{
    glm::vec3 up = getUp();

    glm::vec3 newPosition = position - up * deltaTime * movingSpeed;
    if(newPosition.x >= -aquariumSize.x && newPosition.x <= aquariumSize.x)
    {
        if(newPosition.y >= -aquariumSize.y && newPosition.y <= aquariumSize.y)
        {
            if(newPosition.z >= -aquariumSize.z && newPosition.z <= aquariumSize.z)
            {
                position = newPosition;
            }
        }
    }
}

glm::vec3 Camera::getDirection()
{
    glm::vec3 direction = glm::vec3(cos(verticalAngle) * sin(horizontalAngle),
                                    sin(verticalAngle),
                                    cos(verticalAngle) * cos(horizontalAngle));
    return direction;
}

glm::vec3 Camera::getRight()
{
    glm::vec3 right =  glm::vec3(sin(horizontalAngle - 3.14f/2.0f),
                            0,
                            cos(horizontalAngle - 3.14f/2.0f));

    return right;
}

glm::vec3 Camera::getUp()
{
    glm::vec3 direction = getDirection();

    glm::vec3 right =  getRight();

    glm::vec3 up = glm::cross( right, direction );

    return up;
}



/*--------------------------------------------------------------------------*/


glm::mat4 Camera::getViewMatrix()
{
    glm::vec3 direction = glm::vec3(cos(verticalAngle) * sin(horizontalAngle),
                                    sin(verticalAngle),
                                    cos(verticalAngle) * cos(horizontalAngle));

    glm::vec3 right =  glm::vec3(sin(horizontalAngle - 3.14f/2.0f),
                            0,
                            cos(horizontalAngle - 3.14f/2.0f));

    glm::vec3 up = glm::cross( right, direction );

    if(isArcBall)
    {

        position = arcBallPosition - (glm::normalize(direction) * radius);
        //printf("pozycja kamery %f %f %f", position.x, position.y, position.z);
        ViewMatrix = glm::lookAt(position, glm::vec3(0,0,0), up);
    }
    else
    {
        ViewMatrix = glm::lookAt(position, position + direction, up);
    }


    return ViewMatrix;
}

glm::mat4 Camera::getProjectionMatrix()
{
    ProjectionMatrix = glm::perspective(glm::radians(FoV), float(resolution.x / resolution.y), near, far);

    return ProjectionMatrix;
}
