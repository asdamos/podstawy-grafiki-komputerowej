#ifndef MODELASSET_H
#define MODELASSET_H

#include <GL/glew.h>
#include <ctime>
#include <cmath>
// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

using namespace glm;

#include "shader.h"

#include <cstdlib>

#define MAX_LIGHTS 12

class ModelAsset
{
public:
    virtual void init() = 0;
    virtual void clean() = 0;

    GLuint VAO;
    GLuint * VBO;

    int VBOsNumber;

    GLenum drawType;

    GLint drawStart;
    GLint drawCount;

    GLfloat shinines;
    glm::vec3 specularColor;


    //shader
    GLuint Shader;

    GLuint alphaUniform;
    GLuint colorUniform;

    GLuint ModelMatrixUniform;
    GLuint ViewMatrixUniform;
    GLuint ProjectionMatrixUniform;

    GLuint LightsNumberUniform;

    GLuint LightPositionUniform[MAX_LIGHTS];
    GLuint LightColorUniform[MAX_LIGHTS];
    GLuint LightAmbientCoefficientUniform[MAX_LIGHTS];
    GLuint LightAttenuationUniform[MAX_LIGHTS];

    GLuint ConeAngleUniform[MAX_LIGHTS];
    GLuint ConeDirectionUniform[MAX_LIGHTS];

    GLuint MaterialShininessUniform;
    GLuint MaterialSpecularColorUniform;
    GLuint CameraPositionUniform;


    GLuint IsBonusBubbleUniform;
};

#endif // MODELASSET_H
