#include "sphereasset.h"

#include <GL/glew.h>
#include <ctime>
#include <cmath>
// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

using namespace glm;

#include "shader.h"

#include <cstdlib>

#include <cstdio>
#include <vector>
#include <string>
#include <sstream>


struct triangle
{
    glm::vec3 v1;
    glm::vec3 v2;
    glm::vec3 v3;
};


SphereAsset::SphereAsset(int slicesNumber, int vertexesPerSlice)
{
    this->slicesNumber = slicesNumber;
    this->vertexesPerSlice = vertexesPerSlice;
    this->VBOsNumber = 2;
    this->drawType = GL_TRIANGLES;
    this->drawStart = 0;

    this->shinines = 10.0f;
    this->specularColor = glm::vec3(1.0f, 1.0f, 1.0f);
}

void SphereAsset::init()
{
    Shader = LoadShaders("VS.glsl", "FS.glsl");

    alphaUniform = glGetUniformLocation(Shader, "alpha");
    colorUniform = glGetUniformLocation(Shader, "color");

    ModelMatrixUniform = glGetUniformLocation(Shader, "ModelMatrix");
    ViewMatrixUniform = glGetUniformLocation(Shader, "ViewMatrix");
    ProjectionMatrixUniform = glGetUniformLocation(Shader, "ProjectionMatrix");

    LightsNumberUniform = glGetUniformLocation(Shader, "lightsNumber");

    for(int i=0; i<MAX_LIGHTS; i++)
    {
        std::ostringstream ss;
        std::string name;


        ss << "lights[" << i << "].color";
        name = ss.str();
        ss.str(std::string());
        LightColorUniform[i] = glGetUniformLocation(Shader, name.c_str());


        ss << "lights[" << i << "].position";
        name = ss.str();
        ss.str(std::string());
        LightPositionUniform[i] = glGetUniformLocation(Shader, name.c_str());


        ss << "lights[" << i << "].ambientCoefficient";
        name = ss.str();
        ss.str(std::string());
        LightAmbientCoefficientUniform[i] = glGetUniformLocation(Shader, name.c_str());


        ss << "lights[" << i << "].attenuation";
        name = ss.str();
        ss.str(std::string());
        LightAttenuationUniform[i] = glGetUniformLocation(Shader, name.c_str());


        ss << "lights[" << i << "].coneAngle";
        name = ss.str();
        ss.str(std::string());
        ConeAngleUniform[i] = glGetUniformLocation(Shader, name.c_str());


        ss << "lights[" << i << "].coneDirection";
        name = ss.str();
        ss.str(std::string());
        ConeDirectionUniform[i] = glGetUniformLocation(Shader, name.c_str());
    }

    MaterialShininessUniform = glGetUniformLocation(Shader, "materialShininess");
    MaterialSpecularColorUniform = glGetUniformLocation(Shader, "materialSpecularColor");
    CameraPositionUniform = glGetUniformLocation(Shader, "cameraPosition");

    IsBonusBubbleUniform = glGetUniformLocation(Shader, "isBonusBubble");

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    VBO = new GLuint[2];

    std::vector<triangle> triangles;

    glm::vec3 vertexes[slicesNumber-1][vertexesPerSlice];

    glm::vec3 top = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::vec3 bottom = glm::vec3(0.0f, -1.0f, 0.0f);

    for(int i=1; i<slicesNumber ; i++)
    {
        for(int j=0; j<vertexesPerSlice; j++)
        {
            float theta = float(i)/(slicesNumber) * M_PI;

            float phi = float(j)/vertexesPerSlice * M_PI * 2;

            glm::vec3 vertex;
            vertex.x = sin(theta) * cos(phi);
            vertex.y = cos(theta);
            vertex.z = -sin(theta) * sin(phi);

            vertexes[i-1][j] = vertex;
        }
    }

    for(int i=0; i<vertexesPerSlice; i++)
    {
        triangle tr;
        tr.v1 = top;
        tr.v2 = vertexes[0][i];
        tr.v3 = vertexes[0][(i+1) % vertexesPerSlice];

        triangles.push_back(tr);
    }

    for(int i=1; i<slicesNumber-1; i++)
    {
        int firstSlice = i-1;
        int secondSlice = i;

        for(int j=0; j<vertexesPerSlice; j++)
        {
            triangle tr1;
            tr1.v1 = vertexes[firstSlice][j];
            tr1.v2 = vertexes[secondSlice][j];
            tr1.v3 = vertexes[secondSlice][(j+1) % vertexesPerSlice];

            triangles.push_back(tr1);


            triangle tr2;
            tr2.v1 = vertexes[firstSlice][j];
            tr2.v2 = vertexes[secondSlice][(j+1) % vertexesPerSlice];
            tr2.v3 = vertexes[firstSlice][(j+1) % vertexesPerSlice];

            triangles.push_back(tr2);
        }

    }

    for(int i=0; i<vertexesPerSlice; i++)
    {
        triangle tr;
        tr.v1 = bottom;
        tr.v2 = vertexes[slicesNumber-2][(i+1) % vertexesPerSlice];
        tr.v3 = vertexes[slicesNumber-2][i];

        triangles.push_back(tr);
    }


    GLfloat vertex_buffer_data[triangles.size()*3][3];
    GLfloat vertex_normal_buffer_data[triangles.size()*3][3];

    for(int i=0; i<triangles.size(); i++)
    {
        vertex_buffer_data[i*3][0] = triangles[i].v1.x;
        vertex_buffer_data[i*3][1] = triangles[i].v1.y;
        vertex_buffer_data[i*3][2] = triangles[i].v1.z;

        vertex_buffer_data[i*3 + 1][0] = triangles[i].v2.x;
        vertex_buffer_data[i*3 + 1][1] = triangles[i].v2.y;
        vertex_buffer_data[i*3 + 1][2] = triangles[i].v2.z;

        vertex_buffer_data[i*3 + 2][0] = triangles[i].v3.x;
        vertex_buffer_data[i*3 + 2][1] = triangles[i].v3.y;
        vertex_buffer_data[i*3 + 2][2] = triangles[i].v3.z;

    }

    for(int i=0; i<triangles.size()*3; i++)
    {
        glm::vec3 vertex = glm::vec3(vertex_buffer_data[i][0],vertex_buffer_data[i][1],vertex_buffer_data[i][2]);
        glm::vec3 normal = normalize(vertex);
        vertex_normal_buffer_data[i][0] = normal.x;
        vertex_normal_buffer_data[i][1] = normal.y;
        vertex_normal_buffer_data[i][2] = normal.z;


    }

    this->drawCount = triangles.size() * 3;

    glGenBuffers(1, &VBO[0]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer_data), vertex_buffer_data, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glVertexAttribPointer(
                0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
                3,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );

    glGenBuffers(1, &VBO[1]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_normal_buffer_data), vertex_normal_buffer_data, GL_STATIC_DRAW);

    // 2nd attribute buffer : normals
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    glVertexAttribPointer(
                1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
                3,                                // size
                GL_FLOAT,                         // type
                GL_FALSE,                         // normalized?
                0,                                // stride
                (void*)0                          // array buffer offset
                );


    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);

}

void SphereAsset::clean()
{
    glDeleteBuffers(VBOsNumber, VBO);
    glDeleteVertexArrays(1, &VAO);
    glDeleteProgram(Shader);
    delete[] VBO;
}
