Śmiercionośne bąbelki (Termin: 7-8.XII) 25+5 punktów
W tym zadaniu rozważamy trójwymiarowe sześcienne akwarium, z którego dna w losowych miejscach są generowane bąbelki o losowych wielkościach z ustalonego zakresu. Bąbelki unoszą się ze stałą prędkością do góry i zwiększają co nieco swój rozmiar (np. stopniowo aż do 10% więcej po dotarciu do góry). Celem zadania jest napisanie grywalnej gry gdzie gracz będzie musiał przedostać się przez akwarium ze środka jednej ściany na przeciwległą unikając dotknięcia bąbelków. Po przedostaniu się przez akwarium pokonujemy następny poziom, gdzie ilość generowanych bąbelków jest większa (być może też ich wielkość lub nawet szybkość unoszenia). Gra trwa do momentu kolizji gracza z bąbelkiem, wtedy wyświetlamy punkty itd...

STEROWANIE:

    Obsługa przy pomocy klawiatury i myszy
    Graczem można poruszać strzałkami prawo/lewo/przód/tył, a także góra/dół: PgUp/PgDown.
    Poza tym umożliwiamy poprzez obroty (np. arcball) zmiany kierunku patrzenia (klawisze lub mysz). Czasem warto patrzyć trochę na dół na bąbelki ale i do przodu. Potrzebna jest też możliwość dopasowania kąta widzenia (zoom) aby gracz mógł dobrać optymalną. 

RENDEROWANIE:

    Widok z perspektywy obserwatora przełączany cyklicznie klawiszem tabulacji na widok z zewnątrz na akwarium (bez przednich zasłaniających ścianek tak aby było widać unoszące się bąbelki i gracza)
    Woda w akwarium jest lekko niebieska co możemy uzyskać renderując ścianki na niebiesko ale z intensywnością eksponencjalnie proporcjonalną do odległości od ścianki zgodnie z prawem Beer'a (do policzenia w shaderze).
    Bąbelki to różnokolorowe sfery (stworzone z trójkątów w dostatecznej ilości)
    Gracza możemy wyświetlać też jako sferę albo sześcian o odróżniającym się kolorze.
    Należy zadbać o efektywność renderowania, użyć VBO, zastanowić się nad tym co przesyłamy na kartę graficzną 

OŚWIETLENIE (mniej o 5pkt jeśli pominąć poniższe poza prostym odbiciem rozproszonym):

    Stosujemy model odbicia Phonga ze składowymi odbicia rozproszonym i zwierciadlanym (tak aby rozbłyski były widoczne na bąbelkach)
    Oświetlenie z góry jednym głównym źródłem światła, niezależne od odległości, może być po prostu z jednego kierunku światło słoneczne
    Oświetlenie z punktu obserwatora, które powinno być zależne od odległości i być w innym kolorze. Tak aby zbliżające bąbelki było łatwiej zauważyć.
    Dodatkowo niektóre specjalne bąbelki (tzn ustalona ilość 10-20) świecą swoim kolorem (także zależność od odległości). Może te bąbelki warto dotknąć aby uzyskać punkty bonusowe. 

GRYWALNOŚĆ: dodatkowe 0-5 pkt za wyjątkową grywalność, dobrą estetykę i całokształt
