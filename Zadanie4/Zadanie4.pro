TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    shader.cpp \
    game.cpp \
    camera.cpp \
    modelasset.cpp \
    sphereasset.cpp \
    sphereinstance.cpp \
    planeasset.cpp \
    planeinstance.cpp \
    light.cpp \
    bubble.cpp

HEADERS += \
    game.h \
    camera.h \
    modelasset.h \
    sphereasset.h \
    sphereinstance.h \
    planeasset.h \
    planeinstance.h \
    shader.h \
    light.h \
    bubble.h

DISTFILES += \
    Makefile \
    FS.glsl \
    VS.glsl
