#ifndef PLANEINSTANCE_H
#define PLANEINSTANCE_H
#include "planeasset.h"
#include "camera.h"
#include "light.h"
#include <vector>

class PlaneInstance
{
public:
    PlaneInstance(PlaneAsset *asset, glm::vec3 position, glm::vec3 color, glm::vec3 size, glm::vec3 rotation, float alpha, float rotationAngle);

    PlaneAsset *asset;

    glm::vec3 position;

    glm::vec3 color;

    glm::vec3 size;

    glm::mat4 ModelMatrix;

    void draw(Camera cam, std::vector<Light> lights);

    float alpha;
};

#endif // PLANEINSTANCE_H
