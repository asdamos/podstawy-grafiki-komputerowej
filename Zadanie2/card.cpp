#include "card.h"
#include <cstdio>
#include <GL/glew.h>

#include <ctime>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

Card::Card()
{
    this->used = false;
}
constexpr float Card::colours[6][3];



void Card::setMN(int M, int N)
{
    this->M = M;
        this->N = N;
}

void Card::draw(int programID)
{
    if(this->guessed == false)
    {
        GLint uniColor = glGetUniformLocation(programID, "color");
        GLint uniTrans = glGetUniformLocation(programID, "translation");
        glUniform2f(uniTrans, 2*this->x - M, N - 2*this->y);
        if(this->reversed == true)
        {
            glUniform3f(uniColor, colours[colour][0], colours[colour][1], colours[colour][2]);
            glDrawArrays(GL_TRIANGLE_FAN, 4, 4);

            glUniform3f(uniColor, 0.0f, 0.0f, 0.0f);
            if(this->wz == 0)       //Line
            {
                glDrawArrays(GL_LINES, 8,2);
            }
            else if(this->wz == 1)  //Cross
            {
                glDrawArrays(GL_LINES, 8,2);
                glDrawArrays(GL_LINES, 10,2);
            }
            else if(this->wz == 2)  //Triangle
            {
                glDrawArrays(GL_LINES, 10,2);
                glDrawArrays(GL_LINES, 12,2);
                glDrawArrays(GL_LINES, 14,2);
            }
        }
        else
        {
            glUniform3f(uniColor, 0.0f, 0.0f, 0.0f);

            glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        }
    }
}


