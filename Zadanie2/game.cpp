#include "game.h"

#include <GL/glew.h>
#include <cstdio>
#include <stdio.h>
#include <ctime>

// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>


using namespace glm;

#include "shader.hpp"

Game::Game()
{

}

bool Game::initParametres(int M, int N, int K)
{
    Gettimeofday(&lastKeyTime, NULL);
    Gettimeofday(&viewCardsTime, NULL);
    upKeyReleased = true;
    downKeyReleased = true;
    leftKeyReleased = true;
    rightKeyReleased = true;
    spaceKeyReleased = true;

    isCardReversed = false;
    bothCardReversed = false;
    guessed = false;

    guessedNumber = 0;

    this->M=M;
    this->N=N;
    this->K=K;

    if(K != 3 && K!=6)
    {
        fprintf( stderr, "K must be 3 or 6\n" );
        return false;
    }

    if(M*N % 2 == 1)
    {
        fprintf( stderr, "M*N must be even number\n" );
        return false;
    }

    if(M*N > 2*(K*W))
    {
        fprintf(stderr, "Board size is too big\n");
        return false;
    }

    fprintf(stderr, "Arguments ok\n");
    frameX = 0;
    frameY = 0;
    roundNumber = 0;
    return true;
}

bool Game::initGLFW()
{
    // Initialise GLFW

    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        getchar();
        return false;
    }

    return true;
}

bool Game::createWindow()
{
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    this->window = glfwCreateWindow( 1024, 768, "Adam Sawicki", NULL, NULL);
    if( this->window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        getchar();
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(this->window);

    return true;
}

bool Game::initGLEW()
{
    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        return false;
    }

    return true;
}

void Game::generateCards()
{


    this->cards = new Card*[M];
    for(int i=0; i<M; i++)
    {
        cards[i] = new Card[N];
    }

    bool pattern_used [K][W];


    for(int i=0; i<K; i++)
    {
        for(int j=0; j<W; j++)
        {
            pattern_used[i][j]=false;
        }
    }

    srand(time(NULL));

    int n = this->M * this->N;
    int colour, wz;
    int x, y;
    for(int i=0; i<n/2; i++)
    {
        do
        {
            colour=Game::random(0, K);
            wz=Game::random(0, W);
        }
        while(pattern_used[colour][wz] == true);
        pattern_used[colour][wz] = true;

        for(int j=0; j<2; j++)
        {
            do
            {
                x=Game::random(0, this->M);
                y=Game::random(0, this->N);
            }
            while(cards[x][y].used == true);

            cards[x][y].used=true;
            cards[x][y].colour = colour;
            cards[x][y].wz = wz;
            cards[x][y].y = y;
            cards[x][y].x = x;
            cards[x][y].setMN(M, N);
            cards[x][y].guessed = false;
            cards[x][y].reversed = false;
        }
    }
}


void Game::loop()
{
    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    // Black background
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);


    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Create and compile our GLSL program from the shaders
    programID = LoadShaders( "SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader" );

    GLfloat g_vertex_buffer_data[] = {	//Frame
                                        -0.9f, -0.9f,		//Down left
                                        -0.9f, 0.9f,		//Up left
                                        0.9f,  0.9f,		//Up right
                                        0.9f, -0.9f,		//Down right

                                        //Card
                                        -0.8f, -0.8f,		//Down left
                                        -0.8f, 0.8f,		//Up left
                                        0.8f,  0.8f,		//Up right
                                        0.8f, -0.8f,		//Down right

                                        //Vertical line
                                        0.0f, 0.8f,         //Top
                                        0.0f, -0.8f,         //Bottom

                                        //Horizontal line
                                        -0.8f, 0.0f,        //Left
                                        0.8f, 0.0f,         //Right

                                        //Left side of triangle
                                        -0.8f, 0.0f,
                                        0.0f, 0.8f,

                                        //Right side of triangle
                                        0.8f, 0.0f,
                                        0.0f, 0.8f
                                     };

    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

    float Mscale = 1/float(M);
    float Nscale = 1/float(N);
    GLint uniScale = glGetUniformLocation(programID, "scaling");

    do{

        // Clear the screen
        glClear( GL_COLOR_BUFFER_BIT );

        // Use our shader
        glUseProgram(programID);

        // 1rst attribute buffer : vertices
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
                    0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    2,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    0,                  // stride
                    (void*)0            // array buffer offset
                    );

        glUniform2f(uniScale, Mscale, Nscale);

        drawFrame();

        for(int i=0; i<M; i++)
        {
            for(int j=0; j<N; j++)
            {
                cards[i][j].draw(programID);
            }
        }

        glDisableVertexAttribArray(0);
        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();


        struct timeval now;
        Gettimeofday(&now, NULL);
        if(time_diff_in_ms(viewCardsTime, now) > 500 && bothCardReversed == true) //we want to show the cards
        {

            if(guessed == true)
            {
                cards[x2Reversed][y2Reversed].guessed = true;
                cards[xReversed][yReversed].guessed = true;
                guessed = false;
                guessedNumber++;
            }

            cards[x2Reversed][y2Reversed].reversed = false;
            cards[xReversed][yReversed].reversed = false;
            isCardReversed = false;
            bothCardReversed = false;
        }
        else
        {
            handleInput();
        }

        if(guessedNumber == M*N/2)
        {
            printf("Koniec gry! Wynik: %d\n", roundNumber);
        }

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0  && guessedNumber != M*N/2);
}

void Game::clean()
{
    // Cleanup VBO
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteVertexArrays(1, &VertexArrayID);
    glDeleteProgram(programID);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    for(int i=0; i<M; i++)
    {
        delete [] cards[i];
    }
    delete [] cards;


}

void Gettimeofday(struct timeval *tv, struct timezone *tz)
{
    if (gettimeofday(tv, tz) == -1)
    {
        printf("Gettimeofday error");
        exit(1);
    }
    return;
}

u_int64_t time_diff_in_ms(struct timeval start, struct timeval end)
{
    return (end.tv_sec - start.tv_sec) * 1000 + (end.tv_usec - start.tv_usec) / 1000;
}


int Game::random(int min, int max)
{
    int tmp;
    if (max>=min)
        max-= min;
    else
    {
        tmp= min - max;
        min= max;
        max= tmp;
    }
    return max ? (rand() % max + min) : min;
}

void Game::drawFrame()
{
    GLint uniColor = glGetUniformLocation(programID, "color");
    GLint uniTrans = glGetUniformLocation(programID, "translation");

    glUniform3f(uniColor, 0.5f, 0.5f, 0.5f);
    glUniform2f(uniTrans, 2*frameX - M, N - 2*frameY);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}


void Game::printInformations()
{
    printf("Runda numer: %d\n", roundNumber);
}


void Game::handleInput()
{
    struct timeval now;
    Gettimeofday(&now, NULL);
    if(time_diff_in_ms(lastKeyTime, now) > 33) //we want 30 key presses per second max
    {
        if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && upKeyReleased == true)
        {
            if(frameY == 0)
            {
                frameY = N -1;
            }
            else
                frameY -=1;

            lastKeyTime.tv_sec = now.tv_sec;
            lastKeyTime.tv_usec = now.tv_usec;
            upKeyReleased = false;
        }
        else if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && downKeyReleased == true)
        {
            if(frameY == N - 1)
            {
                frameY = 0;
            }
            else
                frameY +=1;

            lastKeyTime.tv_sec = now.tv_sec;
            lastKeyTime.tv_usec = now.tv_usec;
            downKeyReleased = false;
        }
        else if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && leftKeyReleased == true)
        {
            if(frameX == 0)
            {
                frameX = M - 1;
            }
            else
                frameX -=1;

            lastKeyTime.tv_sec = now.tv_sec;
            lastKeyTime.tv_usec = now.tv_usec;
            leftKeyReleased = false;
        }
        else if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && rightKeyReleased == true)
        {
            if(frameX == M - 1)
            {
                frameX = 0;
            }
            else
                frameX  += 1;

            lastKeyTime.tv_sec = now.tv_sec;
            lastKeyTime.tv_usec = now.tv_usec;
            rightKeyReleased = false;
        }
        else if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_RELEASE && upKeyReleased == false)
        {
            upKeyReleased = true;
            lastKeyTime.tv_sec = now.tv_sec;
            lastKeyTime.tv_usec = now.tv_usec;
        }
        else if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_RELEASE && downKeyReleased == false)
        {
            downKeyReleased = true;
            lastKeyTime.tv_sec = now.tv_sec;
            lastKeyTime.tv_usec = now.tv_usec;
        }
        else if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_RELEASE && leftKeyReleased == false)
        {
            leftKeyReleased = true;
            lastKeyTime.tv_sec = now.tv_sec;
            lastKeyTime.tv_usec = now.tv_usec;
        }
        else if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_RELEASE && rightKeyReleased == false)
        {
            rightKeyReleased = true;
            lastKeyTime.tv_sec = now.tv_sec;
            lastKeyTime.tv_usec = now.tv_usec;
        }
        else if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && spaceKeyReleased == true)
        {
            if(bothCardReversed == false)
            {
                if(cards[frameX][frameY].guessed == false)
                {
                    if(isCardReversed == true)
                    {
                        if(xReversed == frameX && yReversed == frameY)     //We dont reverse card second time
                        {

                        }
                        else                        //We reverse another card
                        {
                            if(wzReversed == cards[frameX][frameY].wz && colourReversed == cards[frameX][frameY].colour)    //We found a pair!
                            {
                                x2Reversed = frameX;
                                y2Reversed = frameY;
                                guessed = true;
                                cards[x2Reversed][y2Reversed].reversed = true;
                                Gettimeofday(&viewCardsTime, NULL);
                                bothCardReversed = true;
                            }
                            else                                                                                            //Cards are different
                            {
                                Gettimeofday(&viewCardsTime, NULL);
                                x2Reversed = frameX;
                                y2Reversed = frameY;
                                cards[x2Reversed][y2Reversed].reversed = true;
                                bothCardReversed = true;
                            }
                            roundNumber++;
                            printInformations();
                        }
                    }
                    else
                    {
                        isCardReversed = true;
                        bothCardReversed = false;
                        cards[frameX][frameY].reversed = true;
                        wzReversed = cards[frameX][frameY].wz;
                        colourReversed = cards[frameX][frameY].colour;
                        xReversed = cards[frameX][frameY].x;
                        yReversed = cards[frameX][frameY].y;
                    }
                }
            }

            lastKeyTime.tv_sec = now.tv_sec;
            lastKeyTime.tv_usec = now.tv_usec;
            spaceKeyReleased = false;
        }
        else if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE && spaceKeyReleased == false)
        {

            spaceKeyReleased = true;
            lastKeyTime.tv_sec = now.tv_sec;
            lastKeyTime.tv_usec = now.tv_usec;
        }

    }
}
