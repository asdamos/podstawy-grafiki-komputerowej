#ifndef GAME_H
#define GAME_H

#include "card.h"
#include <GL/glew.h>
#include <sys/time.h>
// Include GLFW
#include <GLFW/glfw3.h>
#include <errno.h>
#include <glm/glm.hpp>
#include <cstdio>

class Game
{
public:
    Game();

    //Initializers
    bool initParametres(int M, int N, int K);
    bool initGLFW();
    bool createWindow();
    bool initGLEW();

    void generateCards();
    void loop();
    void clean();

    struct timeval lastKeyTime;

    struct timeval viewCardsTime;

    GLuint vertexbuffer;
    GLuint VertexArrayID;
    GLuint programID;

    int frameX, frameY;

    bool upKeyReleased;
    bool downKeyReleased;
    bool leftKeyReleased;
    bool rightKeyReleased;
    bool spaceKeyReleased;

    int M, N;   //size
    int K;      //colours - 3(red, green, blue) or 6(red, green, blue, yellow, magenta, cyan)
    int W=3;    //pattern

    Card ** cards;

    GLFWwindow * window;

    int roundNumber;

    int wzReversed;
    int colourReversed;
    int xReversed;
    int yReversed;

    int x2Reversed;
    int y2Reversed;

    bool guessed;

    int guessedNumber;

    bool isCardReversed;
    bool bothCardReversed;

    void handleInput();

    void drawFrame();
    void printInformations();
private:

    int random(int min, int max);
};


void Gettimeofday(struct timeval *tv, struct timezone *tz);

u_int64_t time_diff_in_ms(struct timeval start, struct timeval end);



#endif // GAME_H
