#ifndef CARD_H
#define CARD_H


class Card
{
public:
    Card();

    void draw(int shaderID);
    bool used;

    int colour;
    int wz;

	int x, y;

    int M, N;

    bool reversed;
    bool guessed;

    void setMN(int M, int N);
    static constexpr float colours[6][3]=
    {
        1.0f, 0.0f, 0.0f,       //RED
        0.0f, 1.0f, 0.0f,       //GREEN
        0.0f, 0.0f, 1.0f,       //BLUE
        1.0f, 1.0f, 0.0f,       //YELLOW
        1.0f, 0.0f, 1.0f,       //MAGENTA
        0.0f, 1.0f, 1.0f        //CYAN
    };

};

#endif // CARD_H
