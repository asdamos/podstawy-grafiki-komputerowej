// Include standard headers
#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
using namespace glm;

#include "shader.hpp"
#include "game.h"

int main(int argc, char * argv[])
{
    if(argc != 4)
    {
        fprintf( stderr, "Wrong arguments. Expected format of arguments: M N K\n" );
        return -1;
    }

    Game memory;
    if(! memory.initParametres(atoi(argv[1]), atoi(argv[2]), atoi(argv[3])))
    {
        return -1;
    }

    if(!memory.initGLFW())
    {
        return -1;
    }

    if(!memory.createWindow())
    {
        return -1;
    }

    if(!memory.initGLEW())
    {
        return -1;
    }

    memory.generateCards();

    memory.loop();

    memory.clean();

    return 0;
}
