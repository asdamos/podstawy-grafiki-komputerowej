#include "planeasset.h"
#include <string>
#include <sstream>
#include <cstdio>

PlaneAsset::PlaneAsset()
{
    this->VBOsNumber = 2;
    this->drawType = GL_TRIANGLES;
    this->drawStart = 0;
    this->drawCount = 6;    //2 triangles

    this->shinines = 80.0f;
    this->specularColor = glm::vec3(1.0f, 1.0f, 1.0f);
}

void PlaneAsset::init()
{
    Shader = LoadShaders("VS.glsl", "FS.glsl");

    alphaUniform = glGetUniformLocation(Shader, "alpha");
    colorUniform = glGetUniformLocation(Shader, "color");


    ModelMatrixUniform = glGetUniformLocation(Shader, "ModelMatrix");
    ViewMatrixUniform = glGetUniformLocation(Shader, "ViewMatrix");
    ProjectionMatrixUniform = glGetUniformLocation(Shader, "ProjectionMatrix");

    LightsNumberUniform = glGetUniformLocation(Shader, "lightsNumber");

    for(int i=0; i<MAX_LIGHTS; i++)
    {
        std::ostringstream ss;
        std::string name;


        ss << "lights[" << i << "].color";
        name = ss.str();
        ss.str(std::string());
        LightColorUniform[i] = glGetUniformLocation(Shader, name.c_str());

        ss << "lights[" << i << "].position";
        name = ss.str();
        ss.str(std::string());
        LightPositionUniform[i] = glGetUniformLocation(Shader, name.c_str());


        ss << "lights[" << i << "].ambientCoefficient";
        name = ss.str();
        ss.str(std::string());
        LightAmbientCoefficientUniform[i] = glGetUniformLocation(Shader, name.c_str());


        ss << "lights[" << i << "].attenuation";
        name = ss.str();
        ss.str(std::string());
        LightAttenuationUniform[i] = glGetUniformLocation(Shader, name.c_str());


        ss << "lights[" << i << "].coneAngle";
        name = ss.str();
        ss.str(std::string());
        ConeAngleUniform[i] = glGetUniformLocation(Shader, name.c_str());


        ss << "lights[" << i << "].coneDirection";
        name = ss.str();
        ss.str(std::string());
        ConeDirectionUniform[i] = glGetUniformLocation(Shader, name.c_str());
    }

    MaterialShininessUniform = glGetUniformLocation(Shader, "materialShininess");
    MaterialSpecularColorUniform = glGetUniformLocation(Shader, "materialSpecularColor");
    CameraPositionUniform = glGetUniformLocation(Shader, "cameraPosition");

    IsBonusBubbleUniform = glGetUniformLocation(Shader, "isBonusBubble");

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    VBO = new GLuint[2];


    GLfloat vertex_buffer_data[] =
    {
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, -1.0f,
        -1.0f, 0.0f, 1.0f,

        1.0f, 0.0f, -1.0f,
        -1.0f, 0.0f, -1.0f,
        -1.0f, 0.0f, 1.0f
    };

    //normal is pointing upwards
    GLfloat vertex_normal_buffer_data[] =
    {
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f
    };

    glGenBuffers(1, &VBO[0]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer_data), vertex_buffer_data, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glVertexAttribPointer(
                0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
                3,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );

    glGenBuffers(1, &VBO[1]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_normal_buffer_data), vertex_normal_buffer_data, GL_STATIC_DRAW);

    // 2nd attribute buffer : normals
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    glVertexAttribPointer(
                1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
                3,                                // size
                GL_FLOAT,                         // type
                GL_FALSE,                         // normalized?
                0,                                // stride
                (void*)0                          // array buffer offset
                );


    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);

}

void PlaneAsset::clean()
{
    glDeleteBuffers(VBOsNumber, VBO);
    glDeleteVertexArrays(1, &VAO);
    glDeleteProgram(Shader);
    delete[] VBO;
}


