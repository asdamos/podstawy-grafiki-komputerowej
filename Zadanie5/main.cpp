// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <ctime>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
using namespace glm;

#include "shader.h"
#include "game.h"

//TODO
//Add text

int main(int argc, char * argv[])
{
    srand( time( NULL ) );
    Game gameObj;

    if(!gameObj.initGLFW())
    {
        return -1;
    }

    if(!gameObj.createWindow())
    {
        return -1;
    }

    if(!gameObj.initGLEW())
    {
        return -1;
    }

    gameObj.initGame();

    gameObj.loadMaps(argc, argv);

    gameObj.initMapCamera();
    gameObj.initGlobeCamera();

    gameObj.loop();

    gameObj.clean();

    return 0;
}
