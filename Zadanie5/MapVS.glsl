#version 330 core

#define M_PI 3.1415926535897932384626433832795

layout(location = 0) in float xMap;
layout(location = 1) in float yMap;
layout(location = 2) in float height;

uniform mat4 MVPMatrix;

uniform float latitude;
uniform float longitude;


out vec3 color;

void main(){

    float scale = 10.0f;

    if(height < 0.0f  )
        color = vec3(0, 0, 1); //blue
    else if (height < 500.0f)
        color = vec3(0, height/500.0f, 0); //->green
    else if (height < 1000.0f)
        color = vec3(height/500.0f-1, 1,0); //->yellow
    else if (height < 2000.0f)
        color = vec3(1, 2 -height/1000.0f, 0); //->red
    else
        color = vec3(1, 1, 1);                //white


    float x = scale*radians((longitude + (xMap)/1200.0f));

    float y = scale*log(tan((float(M_PI)/4) + (radians(latitude + (1201.0f-yMap)/1200.0f)/2)));

    gl_Position =  MVPMatrix * vec4(x, y, 0, 1);
}
