#version 330 core

#define M_PI 3.1415926535897932384626433832795
#define EARTH_RADIUS 6378.14

layout(location = 0) in float xMap;
layout(location = 1) in float yMap;
layout(location = 2) in float height;

uniform mat4 MVPMatrix;

uniform float latitude;
uniform float longitude;


out vec3 color;

void main(){
    float z = 0.0f;
    float x = longitude + (xMap)/1200.0f;
    float y = latitude + (1201.0f-yMap)/1200.0f;

    if(height < 0.0f  )
    {
        color = vec3(0, 0, 1); //blue
    }
    else if (height < 500.0f)
        color = vec3(0, height/500.0f, 0); //->green
    else if (height < 1000.0f)
        color = vec3(height/500.0f-1, 1,0); //->yellow
    else if (height < 2000.0f)
        color = vec3(1, 2 -height/1000.0f, 0); //->red
    else
        color = vec3(1, 1, 1);                //white

    if(height < 0.0)
    {
        z = 0.0;
    }
    else if(height >8000.0)
    {
        z = 8000.0;
    }
    else
    {
        z = height;
    }

    z = z / 1000.0; //Metres to Kilometres
    z = z + EARTH_RADIUS;
    z = z / EARTH_RADIUS;

    float theta = radians(x);

    float phi = radians(y);

    phi = M_PI/2 - phi;
    theta = M_PI/2 - theta;

    vec3 vertex = vec3(cos(theta) * sin(phi)*z, cos(phi)*z, sin(theta)*sin(phi)*z);

    gl_Position =  MVPMatrix * vec4(vertex, 1);


}
