#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


class Camera
{
public:
    Camera();

    void setResolution(glm::ivec2 resolution);
    void setPostion(glm::vec3 position);
    void setHorizontalAngle(float angle);
    void setVerticalAngle(float angle);
    void setFoV(float FoV, float min, float max);

    void computeNewOrientation(double xpos, double ypos, float deltaTime);

    void changeSpeed(double yoffset);

    void moveForward(float deltaTime);
    void moveBackward(float deltaTime);
    void moveLeft(float deltaTime);
    void moveRight(float deltaTime);
    void moveUp(float deltaTime);
    void moveDown(float deltaTime);

    glm::ivec2 resolution;

    glm::vec3 position;

    float horizontalAngle;
    float verticalAngle;
    float FoV;
    float minFoV, maxFoV;

    float near, far;

    float radius = 2.0f;

    float mouseSpeed = 1.0f;
    float movingSpeed;

    float FoVAngleSpeed;

    glm::mat4 ProjectionMatrix;
    glm::mat4 ViewMatrix;
    glm::mat4 ModelMatrix;
    glm::mat4 MVP;

    glm::vec3 getDirection();
    glm::vec3 getRight();
    glm::vec3 getUp();


    glm::mat4 getViewMatrix();

    glm::mat4 getProjectionMatrix();

    glm::vec3 positionInGlobe(float x, float y);

    void setCameraAtPosition(glm::vec2 position);

    void setInitialAngles();

    float getRadius();
    float getTheta();
    float getPhi();

    float getLongitude();
    float getLatitude();

};

#endif // CAMERA_H
