#include "game.h"

#include <GL/glew.h>
#include <ctime>
#include <cmath>
// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "shader.h"
#include <cstdlib>
#include <unistd.h>
#include <iostream>

double scrollOffset;

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset)
{
    scrollOffset = yoffset;
}

Game::Game()
{
    resolution = glm::ivec2(1024, 728);
    upKeyReleased = true;
    downKeyReleased = true;
    leftKeyReleased = true;
    rightKeyReleased = true;
    ctrlKeyReleased = true;
    shiftKeyReleased = true;

    tabKeyWasPressed = false;

}

void Game::initGame()
{
    initShaders();
    glGenVertexArrays(1, &VAO);

    glBindVertexArray(VAO);

    mapActive = true;

    std::vector<float> XVertices;
    std::vector<float> YVertices;

    XVertices.clear();
    YVertices.clear();
    for(int i=0; i<1201; i++)
    {
        for(int j=0; j<1201; j++)
        {
            XVertices.push_back(float(j));
            YVertices.push_back(float(i));
        }
    }

    glGenBuffers(1, &XVBO);
    glBindBuffer(GL_ARRAY_BUFFER, XVBO);
    glBufferData(GL_ARRAY_BUFFER, XVertices.size()*sizeof(int), XVertices.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, XVBO);
    glVertexAttribPointer(
                0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
                1,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );
    glDisableVertexAttribArray(0);

    glGenBuffers(1, &YVBO);
    glBindBuffer(GL_ARRAY_BUFFER, YVBO);
    glBufferData(GL_ARRAY_BUFFER, YVertices.size()*sizeof(int), YVertices.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, YVBO);
    glVertexAttribPointer(
                1,                  // attribute. No particular reason for 0, but must match the layout in the shader.
                1,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );
    glDisableVertexAttribArray(1);

    genLODs();
    LODchanged = true;
    LODauto = true;
    LODLevel = 9;

    genGrid();
}

void Game::initShaders()
{
    MapShader = LoadShaders("MapVS.glsl", "MapFS.glsl");

    MapMVPMatrixUniform = glGetUniformLocation(MapShader, "MVPMatrix");

    MapLatitudeUniform = glGetUniformLocation(MapShader, "latitude");
    MapLongitudeUniform = glGetUniformLocation(MapShader, "longitude");


    GlobeShader = LoadShaders("GlobeVS.glsl", "GlobeFS.glsl");

    GlobeMVPMatrixUniform = glGetUniformLocation(GlobeShader, "MVPMatrix");

    GlobeLatitudeUniform = glGetUniformLocation(GlobeShader, "latitude");
    GlobeLongitudeUniform = glGetUniformLocation(GlobeShader, "longitude");

    GridMapShader = LoadShaders("GridMapVS.glsl", "GridMapFS.glsl");

    GridMapMVPMatrixUniform = glGetUniformLocation(GridMapShader, "MVPMatrix");

    GridGlobeShader = LoadShaders("GridGlobeVS.glsl", "GridGlobeFS.glsl");

    GridGlobeMVPMatrixUniform = glGetUniformLocation(GridGlobeShader, "MVPMatrix");
}

void Game::loadMaps(int argc, char * argv[])
{
    position = glm::vec2(0,0);

    mapsNumber = argc-1;
    maps = new MapChunk[mapsNumber];

    for(int i=1; i<argc; i++)
    {
        printf("Loading file '%s'\n", argv[i]);
        maps[i-1].loadFromFile(argv[i]);
    }

    for(int i=0; i<mapsNumber; i++)
    {
        position.x +=maps[i].longitude;
        position.y +=maps[i].latitude;
    }

    if(mapsNumber > 0)
    {
        position.x  = position.x / mapsNumber;
        position.y  = position.y / mapsNumber;
    }
}


bool Game::initGLFW()
{
    // Initialise GLFW

    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        getchar();
        return false;
    }

    return true;
}

bool Game::createWindow()
{
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    this->window = glfwCreateWindow( resolution.x, resolution.y, "Adam Sawicki", NULL, NULL);
    glfwSetCursorPos(window, resolution.x / 2, resolution.y / 2);

    if( this->window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        getchar();
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(this->window);

    return true;
}

bool Game::initGLEW()
{
    // Initialize GLEW
    glewExperimental = GL_TRUE; // Needed for core profile

    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        return false;
    }

    return true;
}

void Game::loop()
{
    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    glfwSetScrollCallback(window, scroll_callback);

    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

    glEnable(GL_DEPTH_TEST);

    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    //glEnable(GL_CULL_FACE);


    //hack for setting cursor at startup
    glfwSwapBuffers(window);
    glfwPollEvents();
    glfwSetCursorPos(this->window, resolution.x / 2, resolution.y / 2);


    double lastTime = glfwGetTime();
    double startTime = glfwGetTime();

    glUseProgram(MapShader);

    int nbFrames = 0;
    do{

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        double currentTime = glfwGetTime();

        nbFrames++;
        if ( currentTime - startTime >= 1.0 ){ // If last prinf() was more than 1 sec ago
            // printf and reset timer
            printf("FPS: %d\n", nbFrames);

            if(LODauto)
            {
                if(nbFrames < 25)
                {
                    LODLevel++;
                    if(LODLevel > 9)
                    {
                        LODLevel = 9;
                    }
                }
                else if(nbFrames > 55)
                {
                    LODLevel--;
                    if(LODLevel < 1)
                    {
                        LODLevel = 1;
                    }
                }
            }

            nbFrames = 0;
            startTime += 1.0;
        }

        float deltaTime = float(currentTime - lastTime);
        lastTime = currentTime;

        getMouseEvents(deltaTime);
        getKeyboardEvents(deltaTime);

        draw();
        //swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0);
}

void Game::clean()
{
    // Close OpenGL window and terminate GLFW

    delete[] maps;

    glDeleteBuffers(1, &XVBO);
    glDeleteBuffers(1, &YVBO);


    glDeleteProgram(GridMapShader);
    glDeleteProgram(GridGlobeShader);
    glDeleteProgram(MapShader);
    glDeleteProgram(GlobeShader);


    glDeleteBuffers(9, IBO);

    glDeleteVertexArrays(1, &VAO);


    glDeleteBuffers(1, &GridXVBO);
    glDeleteBuffers(1, &GridYVBO);
    glDeleteBuffers(1, &GridIBO);

    glDeleteVertexArrays(1, &GridVAO);


    glfwTerminate();
}

void Game::initMapCamera()
{
    mapCamera.movingSpeed = 15.0f;
    mapCamera.setResolution(this->resolution);
    mapCamera.setCameraAtPosition(position);
    mapCamera.setHorizontalAngle(M_PI);
    mapCamera.setVerticalAngle(0.0f);
    mapCamera.setFoV(45.0f, 20.0f, 150.0f);
}

void Game::initGlobeCamera()
{
    globeCamera.movingSpeed = 5.0f;
    globeCamera.setResolution(this->resolution);
    globeCamera.setCameraAtPosition(position);
    globeCamera.setHorizontalAngle(M_PI);
    globeCamera.setVerticalAngle(0.0f);
    globeCamera.setFoV(45.0f, 20.0f, 150.0f);
}

void Game::draw()
{
    drawMaps();
    drawGrid();
}

void Game::drawMaps()
{
    glBindVertexArray(VAO);


    if(mapActive)
    {
        glUseProgram(MapShader);

        glm::mat4 ProjectionMatrix = mapCamera.getProjectionMatrix();
        glm::mat4 ViewMatrix = mapCamera.getViewMatrix();

        glm::mat4 ModelMatrix = glm::mat4(1);
        glm::mat4 MVPmatrix = ProjectionMatrix * ViewMatrix * ModelMatrix;

        glUniformMatrix4fv(MapMVPMatrixUniform, 1, GL_FALSE, &MVPmatrix[0][0]);
    }
    else
    {
        glUseProgram(GlobeShader);

        glm::mat4 ProjectionMatrix = globeCamera.getProjectionMatrix();
        glm::mat4 ViewMatrix = globeCamera.getViewMatrix();

        glm::mat4 ModelMatrix = glm::mat4(1);
        glm::mat4 MVPmatrix = ProjectionMatrix * ViewMatrix * ModelMatrix;


        glUniformMatrix4fv(GlobeMVPMatrixUniform, 1, GL_FALSE, &MVPmatrix[0][0]);
    }

    glEnableVertexAttribArray(0);


    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO[LODLevel-1]);

    for(int i=0; i<mapsNumber; i++)
    {
        glEnableVertexAttribArray(2);
        glBindBuffer(GL_ARRAY_BUFFER, maps[i].VBO);
        glVertexAttribPointer(
                    2,                  // attribute.
                    1,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    0,                  // stride
                    (void*)0            // array buffer offset
                    );
        drawMap(i);
        glDisableVertexAttribArray(2);
    }
}

void Game::drawMap(int number)
{
    if(mapActive)
    {
        glUniform1f(MapLongitudeUniform, float(maps[number].longitude));
        glUniform1f(MapLatitudeUniform, float(maps[number].latitude));
    }
    else
    {
        glUniform1f(GlobeLongitudeUniform, float(maps[number].longitude));
        glUniform1f(GlobeLatitudeUniform, float(maps[number].latitude));
    }
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO[LODLevel - 1]);

    glDrawElements(
                GL_TRIANGLES,      // mode
                LOD[LODLevel-1].size(),    // count
            GL_UNSIGNED_INT,   // type
            (void*)0           // element array buffer offset
            );
}

void Game::genLODs()
{
    for(int i=0; i<9; i++)
    {
        glGenBuffers(1, &IBO[i]);
    }

    genLOD(LOD[0], 1);
    genLOD(LOD[1], 2);
    genLOD(LOD[2], 3);
    genLOD(LOD[3], 4);
    genLOD(LOD[4], 6);
    genLOD(LOD[5], 12);
    genLOD(LOD[6], 24);
    genLOD(LOD[7], 64);
    genLOD(LOD[8], 128);





    for(int i=0; i<9; i++)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO[i]);

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, LOD[i].size() * sizeof(unsigned int), LOD[i].data(), GL_STATIC_DRAW);
    }


}

void Game::genLOD(std::vector<unsigned int> &LOD, int ratio)
{
    LOD.clear();
    int size = 1200;
    int i=0;
    int j=0;
    for(i=0; i<size; i+=ratio)
    {
        for(j=0; j<size; j+=ratio)
        {
            int xplus = i + ratio;
            int yplus = j + ratio;

            if(xplus > size)
            {
                xplus = size;
            }

            if(yplus > size)
            {
                yplus = size;
            }

            //triangle 1
            LOD.push_back(i*1201 + j);
            LOD.push_back(i*1201 + yplus);
            LOD.push_back(xplus*1201 + yplus);
            //triangle 2
            LOD.push_back(i*1201 + j);
            LOD.push_back(xplus*1201 + yplus);
            LOD.push_back(xplus*1201+j);
        }
    }
}

void Game::genGrid()
{
    glGenVertexArrays(1, &GridVAO);

    glBindVertexArray(GridVAO);


    std::vector<float> XVertices;
    std::vector<float> YVertices;

    XVertices.clear();
    YVertices.clear();
    for(int i=-80; i<81; i++)
    {
        for(int j=-180; j<181; j++)
        {
            XVertices.push_back(float(j));
            YVertices.push_back(float(i));
        }
    }

    glGenBuffers(1, &GridXVBO);
    glBindBuffer(GL_ARRAY_BUFFER, GridXVBO);
    glBufferData(GL_ARRAY_BUFFER, XVertices.size()*sizeof(int), XVertices.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, GridXVBO);
    glVertexAttribPointer(
                0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
                1,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );
    glDisableVertexAttribArray(0);

    glGenBuffers(1, &GridYVBO);
    glBindBuffer(GL_ARRAY_BUFFER, GridYVBO);
    glBufferData(GL_ARRAY_BUFFER, YVertices.size()*sizeof(int), YVertices.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, GridYVBO);
    glVertexAttribPointer(
                1,                  // attribute. No particular reason for 0, but must match the layout in the shader.
                1,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );
    glDisableVertexAttribArray(1);


    glGenBuffers(1, &GridIBO);

    for(int i=0; i<160; i++)
    {
        for(int j=0; j<360; j++)
        {
            //horizontal line
            GridLOD.push_back(i*361 + j);
            GridLOD.push_back(i*361 + j+1);


            //vertical line
            GridLOD.push_back(i*361 + j);
            GridLOD.push_back((i+1)*361 + j);

        }
    }

    //last vertical line
    for(int i=0; i<160; i++)
    {
        GridLOD.push_back(i*361 + 360);
        GridLOD.push_back((i+1)*361 + 360);
    }

    //last horizontal line
    for(int i=0; i<360; i++)
    {
        GridLOD.push_back(160*361 + i);
        GridLOD.push_back(160*361 + i+1);
    }


    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GridIBO);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, GridLOD.size() * sizeof(unsigned int), GridLOD.data(), GL_STATIC_DRAW);

}

void Game::drawGrid()
{
    glBindVertexArray(GridVAO);


    if(mapActive)
    {
        glUseProgram(GridMapShader);

        glm::mat4 ProjectionMatrix = mapCamera.getProjectionMatrix();
        glm::mat4 ViewMatrix = mapCamera.getViewMatrix();

        glm::mat4 ModelMatrix = glm::mat4(1);
        glm::mat4 MVPmatrix = ProjectionMatrix * ViewMatrix * ModelMatrix;

        glUniformMatrix4fv(GridMapMVPMatrixUniform, 1, GL_FALSE, &MVPmatrix[0][0]);
    }
    else
    {
        glUseProgram(GridGlobeShader);

        glm::mat4 ProjectionMatrix = globeCamera.getProjectionMatrix();
        glm::mat4 ViewMatrix = globeCamera.getViewMatrix();

        glm::mat4 ModelMatrix = glm::mat4(1);
        glm::mat4 MVPmatrix = ProjectionMatrix * ViewMatrix * ModelMatrix;


        glUniformMatrix4fv(GridGlobeMVPMatrixUniform, 1, GL_FALSE, &MVPmatrix[0][0]);
    }

    glEnableVertexAttribArray(0);

    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GridIBO);

    glDrawElements(
                GL_LINES,      // mode
                GridLOD.size(),    // count
                GL_UNSIGNED_INT,   // type
                (void*)0           // element array buffer offset
                );

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}

void Game::getMouseEvents(float deltaTime)
{
    double xDouble, yDouble;

    glfwGetCursorPos(this->window, &xDouble, &yDouble);

    if(mapActive)
    {

    }
    else
    {
        globeCamera.computeNewOrientation(xDouble, yDouble, deltaTime);

        if(scrollOffset != 0)
        {
            globeCamera.changeSpeed(scrollOffset);
        }
    }

    glfwSetCursorPos(this->window, resolution.x / 2, resolution.y / 2);

    scrollOffset = 0;
}

void Game::getKeyboardEvents(float deltaTime)
{
    checkUpKey(deltaTime);
    checkDownKey(deltaTime);
    checkLeftKey(deltaTime);
    checkRightKey(deltaTime);
    checkCtrlKey(deltaTime);
    checkShiftKey(deltaTime);

    checkLODKeys();
    checkTabKey();
}

void Game::checkUpKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && downKeyReleased)
    {
        upKeyReleased = false;

        if(mapActive)
        {
            mapCamera.moveUp(deltaTime);
        }
        else
        {
            globeCamera.moveUp(deltaTime);
        }

    }
    else if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_RELEASE)
    {
        upKeyReleased = true;
    }
}

void Game::checkDownKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && upKeyReleased)
    {
        downKeyReleased = false;


        if(mapActive)
        {
            mapCamera.moveDown(deltaTime);
        }
        else
        {
            globeCamera.moveDown(deltaTime);
        }

    }
    else if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_RELEASE)
    {
        downKeyReleased = true;
    }
}

void Game::checkLeftKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && rightKeyReleased)
    {
        leftKeyReleased = false;


        if(mapActive)
        {
            mapCamera.moveLeft(deltaTime);
        }
        else
        {
            globeCamera.moveLeft(deltaTime);
        }

    }
    else if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_RELEASE)
    {
        leftKeyReleased = true;
    }
}

void Game::checkRightKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && leftKeyReleased)
    {
        rightKeyReleased = false;


        if(mapActive)
        {
            mapCamera.moveRight(deltaTime);
        }
        else
        {
            globeCamera.moveRight(deltaTime);
        }

    }
    else if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_RELEASE)
    {
        rightKeyReleased = true;
    }
}

void Game::checkShiftKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS && ctrlKeyReleased)
    {
        shiftKeyReleased = false;


        if(mapActive)
        {
            mapCamera.moveForward(deltaTime);
        }
        else
        {
            globeCamera.moveForward(deltaTime);
        }

    }
    else if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
    {
        shiftKeyReleased = true;
    }
}

void Game::checkCtrlKey(float deltaTime)
{
    if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS && shiftKeyReleased)
    {
        ctrlKeyReleased = false;


        if(mapActive)
        {
            mapCamera.moveBackward(deltaTime);
        }
        else
        {
            globeCamera.moveBackward(deltaTime);
        }

    }
    else if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_RELEASE)
    {
        ctrlKeyReleased = true;
    }
}

void Game::checkLODKeys()
{
    if(glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
    {
        if(LODLevel != 1)
        {
            LODchanged = true;
        }
        LODLevel = 1;
        LODauto = false;


    }
    else if(glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
    {
        if(LODLevel != 2)
        {
            LODchanged = true;
        }
        LODLevel = 2;
        LODauto = false;
    }
    else if(glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
    {
        if(LODLevel != 3)
        {
            LODchanged = true;
        }
        LODLevel = 3;
        LODauto = false;
    }
    else if(glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
    {
        if(LODLevel != 4)
        {
            LODchanged = true;
        }
        LODLevel = 4;
        LODauto = false;
    }
    else if(glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS)
    {
        if(LODLevel != 5)
        {
            LODchanged = true;
        }
        LODLevel = 5;
        LODauto = false;
    }
    else if(glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS)
    {
        if(LODLevel != 6)
        {
            LODchanged = true;
        }
        LODLevel = 6;
        LODauto = false;
    }
    else if(glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS)
    {
        if(LODLevel != 7)
        {
            LODchanged = true;
        }
        LODLevel = 7;
        LODauto = false;
    }

    else if(glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS)
    {
        if(LODLevel != 8)
        {
            LODchanged = true;
        }
        LODLevel = 8;
        LODauto = false;
    }
    else if(glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS)
    {
        if(LODLevel != 9)
        {
            LODchanged = true;
        }
        LODLevel = 9;
        LODauto = false;
    }
    else if(glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS)
    {
        LODauto = true;
    }
}

void Game::checkTabKey()
{
    if(glfwGetKey(window, GLFW_KEY_TAB) == GLFW_PRESS)
    {
        tabKeyWasPressed = true;
    }
    else if(glfwGetKey(window, GLFW_KEY_TAB) == GLFW_RELEASE)
    {
        if(tabKeyWasPressed)
        {
            if(mapActive)
            {
                position = glm::vec2(mapCamera.getLongitude(), mapCamera.getLatitude());
                globeCamera.setCameraAtPosition(position);
                globeCamera.setInitialAngles();
            }
            else
            {
                position = glm::vec2(globeCamera.getLongitude(), globeCamera.getLatitude());
                mapCamera.setCameraAtPosition(position);
            }

            mapActive = !mapActive;


            tabKeyWasPressed = false;
        }
    }
}


