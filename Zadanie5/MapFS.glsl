#version 330 core

#define MAX_LIGHTS 12

in vec3 color;

out vec3 finalColor;


void main()
{
    finalColor = color;
}
