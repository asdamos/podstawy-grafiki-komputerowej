#ifndef MAPCAMERA_H
#define MAPCAMERA_H
#include "camera.h"

class MapCamera : public Camera
{
public:
    MapCamera();

    void moveForward(float deltaTime);
    void moveBackward(float deltaTime);
    void moveLeft(float deltaTime);
    void moveRight(float deltaTime);
    void moveUp(float deltaTime);
    void moveDown(float deltaTime);

    glm::vec3 getDirection();
    glm::vec3 getRight();
    glm::vec3 getUp();
    glm::mat4 getViewMatrix();
    glm::mat4 getProjectionMatrix();


    void setCameraAtPosition(glm::vec2 position);


    float xInMapSpace(float x);
    float yInMapSpace(float y);

    float getLongitude();
    float getLatitude();
};

#endif // MAPCAMERA_H
