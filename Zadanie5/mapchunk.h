#ifndef MAPCHUNK_H
#define MAPCHUNK_H

#include <string>
#include <GL/glew.h>
#include <vector>

class MapChunk
{
public:
    MapChunk();
    ~MapChunk();

    std::string fileName;
    int latitude;   //szerokość geograficzna

    char latitudeC;

    int longitude;  //długość

    char longitudeC;

    void loadFromFile(std::string fileName);

    GLuint VBO;
};

#endif // MAPCHUNK_H
