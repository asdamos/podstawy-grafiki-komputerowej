#include "mapchunk.h"
#include <fstream>
#include <iostream>
#include <arpa/inet.h>
#include <string>
#include <cstdlib>
#include <cctype>

MapChunk::MapChunk()
{

}

MapChunk::~MapChunk()
{
    glDeleteBuffers(1, &VBO);
}

void MapChunk::loadFromFile(std::string fileName)
{
    fileName = fileName;
    std::ifstream file;
    file.open(fileName.c_str(), std::ifstream::in | std::ifstream::binary);

    short tmp;

    std::vector<float> heightsVector;

    for(int i=0; i<1201; i++)
    {
        for(int j=0; j<1201; j++)
        {
            file.read((char *)&tmp,sizeof(short));
            tmp = ntohs(tmp);
            heightsVector.push_back(float(tmp));
        }
    }
    file.close();

    //file name - 11 characters

    int length = fileName.length();

    std::string name = fileName.substr(length-11, 11);

    latitudeC = name[0];
    latitudeC = toupper(latitudeC);

    std::string tmpStr = name.substr(1, 2);

    latitude = atoi(tmpStr.c_str());


    longitudeC = name[3];
    longitudeC = toupper(longitudeC);

    tmpStr = name.substr(4, 3);

    longitude = atoi(tmpStr.c_str());


    if(latitudeC == 'S')
    {
        latitude *=-1;
    }

    if(longitudeC == 'W')
    {
        longitude *=-1;
    }

    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, heightsVector.size()*sizeof(int), heightsVector.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(
                2,                  // attribute. No particular reason for 0, but must match the layout in the shader.
                1,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );
    glDisableVertexAttribArray(2);




}
