#version 330 core

#define M_PI 3.1415926535897932384626433832795
layout(location = 0) in float xMap;
layout(location = 1) in float yMap;

uniform mat4 MVPMatrix;

out vec3 color;

void main(){

    color = vec3(0,0,0);

    float theta = radians(xMap);

    float phi = radians(yMap);

    phi = M_PI/2 - phi;
    theta = M_PI/2 - theta;

     vec3 vertex = vec3(cos(theta) * sin(phi), cos(phi), sin(theta)*sin(phi));

    gl_Position =  MVPMatrix * vec4(vertex, 1);
}
