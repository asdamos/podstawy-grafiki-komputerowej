#ifndef PLANEASSET_H
#define PLANEASSET_H
#include "modelasset.h"

class PlaneAsset : public ModelAsset
{
public:
    PlaneAsset();

    void init();
    void clean();
};

#endif // PLANEASSET_H
