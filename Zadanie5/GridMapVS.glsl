#version 330 core

#define M_PI 3.1415926535897932384626433832795
layout(location = 0) in float xMap;
layout(location = 1) in float yMap;

uniform mat4 MVPMatrix;

out vec3 color;

void main(){

    float scale = 10.0f;


    color = vec3(0, 0, 0);


    float x = scale*radians(xMap);

    float y = scale*log(tan((float(M_PI)/4) + (radians(yMap)/2)));

    gl_Position =  MVPMatrix * vec4(x, y, 0, 1);
}
