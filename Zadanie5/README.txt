Teren
Zadanie 5 na zajęcia z Podstaw Grafiki Komputerowej
Autor: Adam Sawicki
Indeks: 270814

Sterowanie:

Klawisze 1-9 	- zmiana poziomu detali
Klawisz 0	- włączenie automatycznego trybu poziomu detali
Tab		- zmiana trybu wyświetlania

-----------------------------------------------------------------

Tryb mapy:
Lewy shift	- przybliżanie mapy
Lewy ctrl	- oddalanie mapy
Strzałki 	- przesuowanie się po mapie

-----------------------------------------------------------------

Tryb globusa:
Ruch myszą 		- rozglądanie się
Lewy shift		- ruch do przodu
Lewy ctrl		- ruch do tyłu
Strzałka w lewo		- ruch w lewo
Strzałka w prawo	- ruch w prawo
Strzałka w górę		- ruch w górę
Strzałka w dół		- ruch w dół
Rolka myszy	- zmiana szybkości

-----------------------------------------------------------------

O programie:
Ścieżki do plików z mapami należy podać w parametrach wywołania programu.

Zgodnie z prośbą zaimplementowałem początkowe ustawienie kamery zależne od podanych map, a także uzależnienie obu widoków od siebie tzn. gdy przełączymy z widoku mapy na widok 3d będziemy obserwować ten sam kawałek ziemii i analogicznie przy przełączeniu z widoku 3d do 2d.
