TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    shader.cpp \
    game.cpp \
    camera.cpp \
    sphereinstance.cpp \
    mapchunk.cpp \
    mapcamera.cpp

HEADERS += \
    game.h \
    camera.h \
    shader.h \
    mapchunk.h \
    mapcamera.h

DISTFILES += \
    Makefile \
    MapFS.glsl \
    MapVS.glsl \
    GlobeFS.glsl \
    GlobeVS.glsl \
    GridGlobeFS.glsl \
    GridGlobeVS.glsl \
    GridMapFS.glsl \
    GridMapVS.glsl

OTHER_FILES += \
    GlobeFS.glsl \
    GlobeVS.glsl \
    MapFS.glsl \
    MapVS.glsl
