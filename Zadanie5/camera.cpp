#include "camera.h"
#include <cmath>
#include <cstdio>
#define PI 3.14159265


Camera::Camera()
{
    mouseSpeed = 0.1f;
    FoVAngleSpeed = 1.0f;
    movingSpeed = 10.0f;
    horizontalAngle = 0.0f;
    verticalAngle = 0.0f;

    near = 0.00001f;
    far = 100.0f;
}

void Camera::setResolution(glm::ivec2 resolution)
{
    this->resolution = resolution;
}

void Camera::setPostion(glm::vec3 position)
{
    this->position = position;
}

void Camera::setHorizontalAngle(float angle)
{
    this->horizontalAngle = angle;
}

void Camera::setVerticalAngle(float angle)
{
    this->verticalAngle = angle;
}

void Camera::setFoV(float FoV, float min, float max)
{
    this->FoV = FoV;
    this->minFoV = min;
    this->maxFoV = max;
}

void Camera::computeNewOrientation(double xpos, double ypos, float deltaTime)
{
    horizontalAngle += mouseSpeed * deltaTime * float(resolution.x/2 - xpos );
    verticalAngle   += mouseSpeed * deltaTime * float(resolution.y/2 - ypos );
}

void Camera::changeSpeed(double yoffset)
{
    float newSpeed;
    if(yoffset == -1.0f)
    {
        newSpeed = movingSpeed + yoffset*movingSpeed*0.5f;
    }
    else
    {
        newSpeed = movingSpeed + yoffset*movingSpeed;
    }

    if(newSpeed > 10.0f)
    {
        movingSpeed = 10.0f;
    }
    else if(newSpeed < 0.01f)
    {
        movingSpeed = 0.01f;
    }
    else
    {
        movingSpeed = newSpeed;
    }
}

void Camera::moveForward(float deltaTime)
{
    glm::vec3 direction = getDirection();

    glm::vec3 newPosition = position + direction * deltaTime * movingSpeed;

    position = newPosition;
}

void Camera::moveBackward(float deltaTime)
{
    glm::vec3 direction = getDirection();

    glm::vec3 newPosition = position - direction * deltaTime * movingSpeed;

    position = newPosition;
}

void Camera::moveLeft(float deltaTime)
{
    glm::vec3 right = getRight();

    glm::vec3 newPosition = position - right * deltaTime * movingSpeed;

    position = newPosition;

}

void Camera::moveRight(float deltaTime)
{
    glm::vec3 right = getRight();

    glm::vec3 newPosition = position + right * deltaTime * movingSpeed;

    position = newPosition;
}

void Camera::moveUp(float deltaTime)
{
    glm::vec3 up = getUp();

    glm::vec3 newPosition = position + up * deltaTime * movingSpeed;

    position = newPosition;
}

void Camera::moveDown(float deltaTime)
{
    glm::vec3 up = getUp();

    glm::vec3 newPosition = position - up * deltaTime * movingSpeed;

    position = newPosition;
}

glm::vec3 Camera::getDirection()
{
    glm::vec3 direction = glm::vec3(cos(verticalAngle) * sin(horizontalAngle),
                                    sin(verticalAngle),
                                    cos(verticalAngle) * cos(horizontalAngle));
    return direction;
}

glm::vec3 Camera::getRight()
{
    glm::vec3 right =  glm::vec3(sin(horizontalAngle - 3.14f/2.0f),
                                 0,
                                 cos(horizontalAngle - 3.14f/2.0f));

    return right;
}

glm::vec3 Camera::getUp()
{
    glm::vec3 direction = getDirection();

    glm::vec3 right =  getRight();

    glm::vec3 up = glm::cross( right, direction );

    return up;
}


/*--------------------------------------------------------------------------*/


glm::mat4 Camera::getViewMatrix()
{
    glm::vec3 direction = getDirection();

    glm::vec3 up = getUp();

    ViewMatrix = glm::lookAt(position, position + direction, up);

    return ViewMatrix;
}

glm::mat4 Camera::getProjectionMatrix()
{
        ProjectionMatrix = glm::perspective(glm::radians(FoV), float(resolution.x / resolution.y), near, far);
//        ProjectionMatrix = glm::perspective(FoV, float(resolution.x / resolution.y), near, far);

        return ProjectionMatrix;
}

glm::vec3 Camera::positionInGlobe(float x, float y)
{
    float theta = glm::radians(x);

    float phi = glm::radians(y);

    phi = M_PI/2 - phi;
    theta = M_PI/2 - theta;

    glm::vec3 vertex = glm::vec3(cos(theta) * sin(phi), cos(phi), sin(theta)*sin(phi));
    return vertex;
}

void Camera::setCameraAtPosition(glm::vec2 position)
{
    glm::vec3 pos = positionInGlobe(position.x, position.y);

    pos.x *=2;
    pos.y *=2;
    pos.z *=2;
    this->position = pos;
}

void Camera::setInitialAngles()
{
    horizontalAngle = M_PI +  M_PI/2  - atan(position.y / position.x);

    horizontalAngle = -getTheta();

    verticalAngle = getPhi() - M_PI;

}

float Camera::getRadius()
{
    return sqrt(position.x * position.x + position.y * position.y + position.z * position.z);
}

float Camera::getTheta()
{
   glm::vec3 pos = glm::normalize(position);
   return (atan2(position.z,position.x) + M_PI/2);
}

float Camera::getPhi()
{
    glm::vec3 pos = glm::normalize(position);
    return (acos(position.y/getRadius()) + M_PI/2);
}

float Camera::getLongitude()
{
    return glm::degrees(M_PI - getTheta());
}

float Camera::getLatitude()
{
    return glm::degrees(M_PI - getPhi());
}

