#include "mapcamera.h"
#include <cstdio>
#include <cmath>
MapCamera::MapCamera()
{

}

void MapCamera::moveForward(float deltaTime)
{
    glm::vec3 vector = glm::vec3(0.0f, 0.0f, 1.0f);

    glm::vec3 newPosition = position - vector * deltaTime * movingSpeed;

    if(newPosition.z < 1.0f)
    {
        newPosition.z = 1.0f;
    }
    position = newPosition;
}

void MapCamera::moveBackward(float deltaTime)
{
    glm::vec3 vector = glm::vec3(0.0f, 0.0f, 1.0f);

    glm::vec3 newPosition = position + vector * deltaTime * movingSpeed;

    if(newPosition.z > 80.0f)
    {
        newPosition.z = 80.0f;
    }
    position = newPosition;
}

void MapCamera::moveLeft(float deltaTime)
{
    glm::vec3 vector = glm::vec3(1.0f, 0.0f, 0.0f);

    float speed;

    if(position.z < movingSpeed)
    {
        speed = position.z;
    }
    else
    {
        speed = movingSpeed;
    }
    glm::vec3 newPosition = position - vector * deltaTime * speed;

    if(newPosition.x < xInMapSpace(-180.0f))
    {
        newPosition.x = xInMapSpace(-180.0f);
    }

    position = newPosition;
}

void MapCamera::moveRight(float deltaTime)
{
    glm::vec3 vector = glm::vec3(1.0f, 0.0f, 0.0f);

    float speed;

    if(position.z < movingSpeed)
    {
        speed = position.z;
    }
    else
    {
        speed = movingSpeed;
    }

    glm::vec3 newPosition = position + vector * deltaTime * speed;

    if(newPosition.x > xInMapSpace(180.0f))
    {
        newPosition.x = xInMapSpace(180.0f);
    }


    position = newPosition;
}

void MapCamera::moveUp(float deltaTime)
{
    glm::vec3 vector = glm::vec3(0.0f, 1.0f, 0.0f);

    float speed;

    if(position.z < movingSpeed)
    {
        speed = position.z;
    }
    else
    {
        speed = movingSpeed;
    }

    glm::vec3 newPosition = position + vector * deltaTime * speed;

    if(newPosition.y > yInMapSpace(80.0f))
    {
        newPosition.y = yInMapSpace(80.0f);
    }

    position = newPosition;
}

void MapCamera::moveDown(float deltaTime)
{
    glm::vec3 vector = glm::vec3(0.0f, 1.0f, 0.0f);

    float speed;

    if(position.z < movingSpeed)
    {
        speed = position.z;
    }
    else
    {
        speed = movingSpeed;
    }

    glm::vec3 newPosition = position - vector * deltaTime * speed;

    if(newPosition.y < yInMapSpace(-80.0f))
    {
        newPosition.y = yInMapSpace(-80.0f);
    }

    position = newPosition;
}

glm::vec3 MapCamera::getDirection()
{
    glm::vec3 direction = glm::vec3(cos(verticalAngle) * sin(horizontalAngle),
                                    sin(verticalAngle),
                                    cos(verticalAngle) * cos(horizontalAngle));
    return direction;
}

glm::vec3 MapCamera::getRight()
{
    glm::vec3 right =  glm::vec3(sin(horizontalAngle - 3.14f/2.0f),
                                 0,
                                 cos(horizontalAngle - 3.14f/2.0f));

    return right;
}

glm::vec3 MapCamera::getUp()
{
    glm::vec3 direction = getDirection();

    glm::vec3 right =  getRight();

    glm::vec3 up = glm::cross( right, direction );

    return up;
}



/*--------------------------------------------------------------------------*/


glm::mat4 MapCamera::getViewMatrix()
{
    glm::vec3 direction = glm::vec3(cos(verticalAngle) * sin(horizontalAngle),
                                    sin(verticalAngle),
                                    cos(verticalAngle) * cos(horizontalAngle));

    glm::vec3 right =  glm::vec3(sin(horizontalAngle - 3.14f/2.0f),
                                 0,
                                 cos(horizontalAngle - 3.14f/2.0f));

    glm::vec3 up = glm::cross( right, direction );

    ViewMatrix = glm::lookAt(position, position + direction, up);

    return ViewMatrix;
}

glm::mat4 MapCamera::getProjectionMatrix()
{
    ProjectionMatrix = glm::perspective(glm::radians(FoV), float(resolution.x / resolution.y), near, far);
//    ProjectionMatrix = glm::perspective(FoV, float(resolution.x / resolution.y), near, far);

    return ProjectionMatrix;
}

void MapCamera::setCameraAtPosition(glm::vec2 position)
{
    float x = xInMapSpace(position.x);

    float y = yInMapSpace(position.y);

    this->position = glm::vec3(x, y, 5.0f);
}

float MapCamera::xInMapSpace(float x)
{
    float scale = 10.0f;
    return scale*glm::radians(x);
}

float MapCamera::yInMapSpace(float y)
{
    float scale = 10.0f;
    return scale*log(tan((float(M_PI)/4) + (glm::radians(y)/2)));
}

float MapCamera::getLongitude()
{
    float x = position.x;
    float scale = 10.0f;
    return glm::degrees(x/scale);
}

float MapCamera::getLatitude()
{
    float y = position.y;
    float scale = 10.0f;
    return glm::degrees(2*atan(exp(y/scale)) - M_PI/2);
}

