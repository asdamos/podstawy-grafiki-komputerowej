#ifndef GAME_H
#define GAME_H

#include <GL/glew.h>
#include <sys/time.h>
// Include GLFW
#include <GLFW/glfw3.h>
#include <errno.h>
#include <glm/glm.hpp>
#include <cstdio>

#include "camera.h"
#include "mapcamera.h"

#include "mapchunk.h"
#include <vector>

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);


class Game
{
public:
    Game();

    //Initializers
    bool initGLFW();
    bool createWindow();
    bool initGLEW();

    void initGame();

    void initShaders();

    void loadMaps(int argc, char * argv[]);

    void loop();
    void clean();

    void initMapCamera();
    void initGlobeCamera();

    void draw();


    void drawMaps();
    void drawMap(int number);

    void genLODs();
    void genLOD(std::vector<unsigned int> & LOD, int ratio);


    void genGrid();
    void genGridLOD(std::vector<unsigned int> & LOD, int ratio);


    void drawGrid();


    glm::vec2 position;

    GLFWwindow * window;

    MapChunk * maps;

    MapCamera mapCamera;
    Camera globeCamera;

    int mapsNumber;

    bool mapActive;

    void switchCameras();

    //parametres

    glm::ivec2 resolution;

    //Maps

    GLuint VAO;
    GLuint XVBO;
    GLuint YVBO;


    //Shaders

    GLuint MapShader;

    GLuint MapMVPMatrixUniform;
    GLuint MapLatitudeUniform;
    GLuint MapLongitudeUniform;

    GLuint GlobeShader;

    GLuint GlobeMVPMatrixUniform;
    GLuint GlobeLatitudeUniform;
    GLuint GlobeLongitudeUniform;

    GLuint GridMapShader;

    GLuint GridMapMVPMatrixUniform;

    GLuint GridGlobeShader;

    GLuint GridGlobeMVPMatrixUniform;

    int LODLevel;
    bool LODchanged;
    bool LODauto;

    std::vector<unsigned int> LOD[9];
    GLuint IBO[9];

    GLuint GridVAO;
    GLuint GridXVBO;
    GLuint GridYVBO;
    GLuint GridIBO;
    std::vector<unsigned int> GridLOD;


    void getMouseEvents(float deltaTime);
    void getKeyboardEvents(float deltaTime);


    bool upKeyReleased;
    bool downKeyReleased;
    bool leftKeyReleased;
    bool rightKeyReleased;
    bool shiftKeyReleased;
    bool ctrlKeyReleased;

    bool tabKeyWasPressed;

    void checkUpKey(float deltaTime);
    void checkDownKey(float deltaTime);
    void checkLeftKey(float deltaTime);
    void checkRightKey(float deltaTime);
    void checkShiftKey(float deltaTime);
    void checkCtrlKey(float deltaTime);
    void checkLODKeys();
    void checkTabKey();


};

#endif // GAME_H
